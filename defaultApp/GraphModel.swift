//
//  GraphPointModel.swift
//  dCounterApp
//
//  Created by fujii on 2021/01/23.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

struct GraphPoint {
    let x: CGFloat
    let y: CGFloat
    var xLabel: String
}

class GraphModel {

    class func createGraphArray(dateArray: Array<Date>, countArray: Array<Double>) -> Array<GraphPoint> {
        if dateArray.count == 0 {
            return Array<GraphPoint>()
        }

        var idxArray = Array<Int>()
        var dateStringArray = Array<String>()
        for i in 0 ..< dateArray.count {
            idxArray.append(i)
            dateStringArray.append(stringFromDate(dateArray[i]))
        }
        idxArray.sort{dateArray[$0].compare(dateArray[$1]) == .orderedAscending}
        if 10 < idxArray.count {
            idxArray = idxArray.suffix(10).map{$0}
        }

        var graphArray = Array<GraphPoint>()
        let firstDate = dateArray[idxArray[0]]
        for idx in idxArray {
            let graphPoint = GraphPoint(
                x: CGFloat(diffDays(firstDate, dateArray[idx])),
                y: CGFloat(countArray[idx]),
                xLabel: dateStringArray[idx])
            graphArray.append(graphPoint)
        }
        return graphArray
    }

    class func createGraphDailyArray(_ dataModel: AppDataModel) -> Array<GraphPoint> {
        var dateArray = Array<Date>()
        var countArray = Array<Double>()
        for i in 0 ..< dataModel.dailyTabListSize() {
            dateArray.append(dateFromString(dataModel.dailyTabList(i).strDate)!)
            countArray.append(dataModel.dailyTabList(i).count)
        }
        return self.createGraphArray(dateArray: dateArray, countArray: countArray)
    }

    class func createGraphMonthArray(_ dataModel: AppDataModel) -> Array<GraphPoint> {
        let pair = dataModel.countsByMonth()
        var graphArray = self.createGraphArray(dateArray: pair.0, countArray: pair.1)
        for idx in 0 ..< graphArray.count {
            graphArray[idx].xLabel = graphArray[idx].xLabel.substring(to: 7)
        }
        return graphArray
    }

    class func createGraphWeekArray(_ dataModel: AppDataModel) -> Array<GraphPoint> {
        let pair = dataModel.countsByWeek()
        return self.createGraphArray(dateArray: pair.0, countArray: pair.1)
    }
    
    class func yMaxText(_ dataModel: AppDataModel, _ graphArray : Array<GraphPoint>) -> String {
        let maxCount = self.maxCount(graphArray)
        if dataModel.isTimeMode {
            return convStrTime(second: Int(maxCount))
        }
        return maxCount.dispString()
    }

    class func yMinText(_ dataModel: AppDataModel, _ graphArray : Array<GraphPoint>) -> String {
        let fixMinCount = self.minCount(graphArray)
        if dataModel.isTimeMode {
            return convStrTime(second: Int(fixMinCount))
        }
        return fixMinCount.dispString()
    }

    class func maxCount(_ graphArray : Array<GraphPoint>) -> Double {
        return Double(graphArray.map { $0.y }.max() ?? 0)
    }

    class func minCount(_ graphArray : Array<GraphPoint>) -> Double {
        let maxCount = self.maxCount(graphArray)
        let minCount = Double(graphArray.map { $0.y }.min() ?? 0)
        if (minCount < maxCount && (minCount < 0 || maxCount < minCount * 2)) {
            return minCount
        }
        return 0.0
    }

}
