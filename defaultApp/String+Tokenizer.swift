//
//  String+Tokenizer.swift
//  defaultApp
//
//  Created by fujii on 2020/11/23.
//  Copyright © 2020 fujii. All rights reserved.
//

import UIKit

extension String {

    func convHiragana() -> String {
        var result: String = ""

        let jaLocaleIdentifier = CFLocaleCreateCanonicalLanguageIdentifierFromString(kCFAllocatorDefault, "ja" as CFString)
        let locale = CFLocaleCreate(kCFAllocatorDefault, jaLocaleIdentifier)

        let tokenizer = CFStringTokenizerCreate(kCFAllocatorDefault,
                                                self as CFString, CFRangeMake(0, (self as NSString).length),
                                                kCFStringTokenizerUnitWordBoundary, locale)
        var tokenType: CFStringTokenizerTokenType = CFStringTokenizerGoToTokenAtIndex(tokenizer, 0)

        while tokenType != [] {
            if let latin = CFStringTokenizerCopyCurrentTokenAttribute(tokenizer, kCFStringTokenizerAttributeLatinTranscription) {
                var token: String
                if 0 < (tokenType.rawValue & CFStringTokenizerTokenType.isCJWordMask.rawValue) {
                    token = latin.applyingTransform(.latinToHiragana, reverse: false) ?? ""
                } else if 0 < (tokenType.rawValue & CFStringTokenizerTokenType.hasNonLettersMask.rawValue) {
                    token = " "
                } else {
                    let range = CFStringTokenizerGetCurrentTokenRange(tokenizer)
                    token = ((self as NSString).substring(with: NSMakeRange(range.location, range.length)) as String).lowercased()
                }
                result.append(token)
            }
            tokenType = CFStringTokenizerAdvanceToNextToken(tokenizer)
        }

        return result
    }

}
