//
//  ItemHeaderTableViewCell.swift
//  dCounterApp
//
//  Created by fujii on 2021/03/12.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

class ItemHeaderTableViewCell: UITableViewCell {
    @IBOutlet var countInfoLabel: UILabel!
    @IBOutlet var dateTermLabel: UILabel!
    @IBOutlet var dateTermView: UIView!
    @IBOutlet var totalCountLabel: UILabel!
    @IBOutlet var totalInfoView: UIView!
    @IBOutlet var avgCountLabel: UILabel!
    @IBOutlet var totalAvgInfoView: UIView!
    @IBOutlet var minCountLabel: UILabel!
    @IBOutlet var maxCountLabel: UILabel!
    @IBOutlet var minMaxInfoView: UIView!

    func setupView(dataModel: AppDataModel, isShowDetail:Bool) {
        let dateInfo = dataModel.dateInfoOfAll()
        self.countInfoLabel.text = ItemViewModel.headerCountText(dataModel: dataModel, dateInfo: dateInfo)

        if !isShowDetail {
            self.dateTermView.isHidden = true
            self.totalAvgInfoView.isHidden = true
            self.minMaxInfoView.isHidden = true
            return
        }
        
        self.dateTermView.isHidden = false
        self.dateTermLabel.text = ItemViewModel.headerDateTermText(dateInfo: dateInfo)

        if dataModel.isCulcAvg {
            self.totalInfoView.isHidden = true
        } else {
            self.totalInfoView.isHidden = false
            self.totalCountLabel.text = AppDataModel.countText(count: dataModel.totalCount, isTimeMode: dataModel.isTimeMode)
        }
        self.totalAvgInfoView.isHidden = false
        self.avgCountLabel.text = AppDataModel.countText(count: dataModel.averageOfAll(), isTimeMode: dataModel.isTimeMode)

        let minText = AppDataModel.countText(count: dataModel.minOfAll(), isTimeMode: dataModel.isTimeMode)
        let maxText = AppDataModel.countText(count: dataModel.maxOfAll(), isTimeMode: dataModel.isTimeMode)
        if minText != maxText {
            self.minCountLabel.text = minText
            self.maxCountLabel.text = maxText
            self.minMaxInfoView.isHidden = false
        } else {
            self.minMaxInfoView.isHidden = true
        }
    }

}
