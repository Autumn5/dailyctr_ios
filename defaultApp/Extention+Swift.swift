//
//  Extention+Swift.swift
//  dCounterApp
//
//  Created by fujii on 2021/02/11.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

extension Double {

    public func dispString() -> String {
        if self > self.rounded(.down) {
            return (Decimal((self * 100).rounded(.toNearestOrAwayFromZero)) / Decimal(100)).description
        }
        return Int(self).description
    }

    public func fix() -> Double {
        if self > self.rounded(.down) {
            return (self * 100).rounded(.toNearestOrAwayFromZero) / 100
        }
        return self
    }

}
