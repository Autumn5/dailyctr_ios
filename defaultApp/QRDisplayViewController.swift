//
//  QRDisplayViewController.swift
//  defaultApp
//
//  Created by fujii on 2021/01/11.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

class QRDisplayViewController: UIViewController {
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var countInfoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dataModel = AppDataModel()
        dataModel.loadVariable()
        let qrData = dataModel.createQRData()
        self.qrImageView.image = self.createQRImage(qrData.qrString)
        self.countInfoLabel.text = self.countInfoText(size: qrData.count,
                                                      allSize: dataModel.dailyTabListSize())
    }

    func createQRImage(_ text: String) -> UIImage? {
        guard let data = text.data(using: .utf8) else { return nil }
        guard let QR = CIFilter(name: "CIQRCodeGenerator", parameters: ["inputMessage": data]) else { return nil }
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        guard let ciImage = QR.outputImage?.transformed(by: transform) else { return nil }
        guard let cgImage = CIContext().createCGImage(ciImage, from: ciImage.extent) else { return nil }
        return UIImage(cgImage: cgImage)
    }

    func countInfoText(size: Int, allSize: Int) -> String {
        let prefix: String
        if allSize == size {
            prefix = "全"
        } else {
            prefix = "直近"
        }
        return String(format: "%@%d件のデータ(最大%dKB)", prefix, size, MAX_QR_SIZE/1024)
    }
    
    // MARK: - IBActions
    
    @IBAction func onTapCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }

}
