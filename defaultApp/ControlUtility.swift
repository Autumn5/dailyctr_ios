//
//  ControlUtility.swift
//  dCounterApp
//
//  Created by fujii on 2021/01/24(v0.0.1).
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

// MARK: - UISegmentedControl

func removeSegments(_ segment: UISegmentedControl, maxSegments: Int) {
    if maxSegments < segment.numberOfSegments {
        let removeCount = segment.numberOfSegments - maxSegments
        for _ in 0 ..< removeCount {
            segment.removeSegment(at: maxSegments, animated: false)
        }
    }
}

func updateSegments(_ segment: UISegmentedControl, tabNames:Array<String>, selectedIndex: Int) {
    for i in 0 ..< tabNames.count {
        if segment.numberOfSegments <= i {
            segment.insertSegment (withTitle: tabNames[i], at: segment.numberOfSegments, animated: false)
        } else {
            segment.setTitle(tabNames[i], forSegmentAt: i)
        }
    }
    removeSegments(segment, maxSegments: tabNames.count)
    segment.selectedSegmentIndex = selectedIndex
}

// MARK: - UIColor

// UIColorに変換
func colorWithIntColor(_ intColor: Int)-> UIColor {
    return UIColor.init(
        red: CGFloat((intColor & 0xFF0000) >> 16) / 255,
        green: CGFloat((intColor & 0x00FF00) >> 8) / 255,
        blue: CGFloat(intColor & 0x0000FF) / 255, alpha: 1)
}

func margeColor(intColor1: Int, intColor2: Int)-> UIColor {
    return UIColor.init(
        red: CGFloat(((intColor1 & 0xFF0000) >> 16) + ((intColor2 & 0xFF0000) >> 16)) / 2 / 255,
        green: CGFloat(((intColor1 & 0x00FF00) >> 8) + ((intColor2 & 0x00FF00) >> 8)) / 2 / 255,
        blue: CGFloat((intColor1 & 0x0000FF) + (intColor2 & 0x0000FF)) / 2 / 255, alpha: 1)
}

func isDarkTheme() -> Bool {
    return (UITraitCollection.current.userInterfaceStyle == .dark)
}

// 背景色を返す
// @param color 0=デフォルト背景色に変換
func backColor(_ color: Int) -> UIColor {
    if color == 0 {
        return UIColor.systemBackground
    }
    if (isDarkTheme()) {
        return margeColor(intColor1: color, intColor2: 0x101010)
    }
    return colorWithIntColor(color)
}

// MARK: - UIButton

// ボタンの背景色を背景画像に変換する
func setBackgroundImage(button: UIButton) {
    let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
    UIGraphicsBeginImageContext(rect.size)
    let context = UIGraphicsGetCurrentContext()!
    context.setFillColor(button.backgroundColor!.cgColor)
    context.fill(rect)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    button.setBackgroundImage(image, for: UIControl.State.normal)
}
