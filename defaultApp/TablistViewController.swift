//
//  TablistViewController.swift
//  dCounterApp
//
//  Created by fujii on 2021/03/03.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

protocol TablistViewControllerDelegate: class {
    func tablistViewControllerUpdate()
}

class TablistViewController: UIViewController
    , UITableViewDelegate, UITableViewDataSource
    , TablistTableViewCellDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var editButton: UIButton!
    public weak var delegate: TablistViewControllerDelegate!
    var dataModel: AppDataModel!
    var rowList: Array<Int> = Array()
    var filterText: String = ""
    var filterList: Array<String>!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.registerNib(withNibName: String(describing: TablistTableViewCell.self))

        self.dataModel = AppDataModel()
        self.loadVariable()

        self.filterList = dataModel.tabNames.prefix(dataModel.tabCount).map{$0.convHiragana()};
        self.createRowList()
        
        self.searchTextField.setLeftImage(UIImage(systemName: "magnifyingglass"), marginLeft: 8)
        
        // ボタンがすぐハイライトするようにする
        self.tableView.delaysContentTouches = false
    }

    // actions

    @IBAction func editingChangedSearchTextField(_ textField: UITextField) {
        if textField.markedTextRange != nil && !textField.text(in: textField.markedTextRange!)!.isOnlyHiragana() {
            return
        }
        self.setFilterText(filterText: textField.text!)
    }
    
    @IBAction func didEndOnExitSearchTextField() {
    }

    func onTapRowItem(dataIndex: Int) {
        self.dataModel.selectedIndex = dataIndex
        self.dataModel.saveVariable()
        self.delegate?.tablistViewControllerUpdate()
        self.dismiss(animated: true, completion: nil)
    }

    func onTapRowCountButton(dataIndex: Int) {
        self.dataModel.updateCount(tabIndex: dataIndex, increment: self.dataModel.tabDatas[dataIndex].increment)
        self.dataModel.saveVariable()
        self.tableView.reloadData()
    }

    @IBAction func onTapEditButton() {
        self.tableView.isEditing = !self.tableView.isEditing
        if self.tableView.isEditing {
            self.editButton.setTitle(string_title_done, for: .normal)
        } else {
            self.editButton.setTitle(string_title_edit, for: .normal)
        }
        self.tableView.reloadData()
    }

    // MARK: - tableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rowList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: String(describing: TablistTableViewCell.self), for: indexPath)
                as! TablistTableViewCell
        cell.setupView(tabIndex: self.rowList[indexPath.row], dataModel: self.dataModel,
                       isEditing: self.tableView.isEditing,
                       delegate: self)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.onTapRowItem(dataIndex: self.rowList[indexPath.row])
    }
    
    // MARK: tableView - edit
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        self.moveItem(fromPosition: self.rowList[sourceIndexPath.row], toPosition: self.rowList[destinationIndexPath.row])
    }    

    // functions - override AppDataModel

    func moveItem(fromPosition:Int, toPosition:Int) {
        if fromPosition == toPosition {
            return
        }
        dataModel.moveTab(fromIndex: fromPosition, toIndex: toPosition)
        dataModel.saveVariable()
        self.moveFilterList(fromIndex: fromPosition, toIndex: toPosition)
        self.createRowList()
        self.tableView.reloadData()
    }

    func loadVariable() {
        self.dataModel.loadVariable()
    }

    // functions - click rows

    func setFilterText(filterText: String) {
        self.filterText = filterText
        self.createRowList()
        self.tableView.reloadData()
    }

    // functions

    func createRowList() {
        self.rowList = TablistViewModel.createRowList(list: self.filterList,
                                                      filterText: self.filterText.convHiragana())
    }

    func moveFilterList(fromIndex: Int, toIndex: Int) {
        let moveRow = self.filterList[fromIndex]
        self.filterList.remove(at: fromIndex)
        self.filterList.insert(moveRow, at: toIndex)
    }
}
