//
//  HomeTabSettingView.swift
//  dCounterApp
//
//  Created by fujii on 2021/02/22.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

enum CountModeType : Int {
    case COUNT; case TIME; case UPDATE
}

enum CulcModeType : Int {
    case TOTAL; case AVG; case MAX
}

protocol HomeTabSettingViewDelegate {
    func tabSettingUpdate()
}

class HomeTabSettingView: UIView
    ,PickerViewDelegate {
    @IBOutlet var tabNameTextField: UITextField!
    @IBOutlet var countModeLabel: UILabel!
    @IBOutlet var culcModeLabel: UILabel!
    @IBOutlet var backColorLabel: UILabel!
    var countModePickerView: PickerView!
    var culcModePickerView: PickerView!
    var backColorPickerView: PickerView!
    var backColorPickerViewDelegate: PickerViewColorDelegate!
    var delegate: HomeTabSettingViewDelegate!
    var dataModel: AppDataModel!
    var colors: Array<Int>!

    class func instance(delegate: HomeTabSettingViewDelegate!
                        ,dataModel: AppDataModel) -> Self {
        let view = (UIView.loadView(nibName: String(describing: self), owner: nil) as! Self)
        view.delegate = delegate
        view.dataModel = dataModel
        view.setup()
        return view
    }
    
    func setup() {
        self.tabNameTextField.text = self.dataModel.tabName
        self.countModePickerView = PickerView.instance(delegate: self)
        self.countModePickerView.setDatas(["カウント", "時間", "更新"])
        self.countModePickerView.selectRow(self.countMode().rawValue)
        self.countModeLabel.text = self.countModePickerView.selectedText()
        self.culcModePickerView = PickerView.instance(delegate: self)
        self.culcModePickerView.setDatas(["合計", "平均"])
        self.culcModePickerView.selectRow(self.culcMode().rawValue)
        self.culcModeLabel.text = self.culcModePickerView.selectedText()

        self.colors = [0,
                0xFFC0C0,
                0xFFFFC0,
                0xC0FFC0,
                0xC0FFFF,
                0xC0C0FF,
                0xFFC0FF]
        self.backColorPickerView = PickerView.instance(delegate: self)
        self.backColorPickerView.setDatas(["デフォルト", "赤", "黄色", "緑", "水色", "青", "桃色"])
        self.backColorPickerView.selectRow(self.colorPosition())
        self.dicidePickerInput(pickerView: self.backColorPickerView)
        self.backColorPickerViewDelegate = PickerViewColorDelegate
            .instance(pickerView: self.backColorPickerView, colors: self.colors)
    }
    
    // MARK: - actions
    
    @IBAction func tabNameTextFieldDidEndOnExit() {
    }
    
    @IBAction func onTapCountModeSelectButton() {
        self.endAllInputing()
        self.parentViewController?.view
            .addSubview(self.countModePickerView, constant: 0)
    }
    
    @IBAction func onTapCulcModeSelectButton() {
        self.endAllInputing()
        self.parentViewController?.view
            .addSubview(self.culcModePickerView, constant: 0)
    }
    
    @IBAction func onTapBackColorSelectButton() {
        self.endAllInputing()
        self.parentViewController?.view
            .addSubview(self.backColorPickerView, constant: 0)
    }
    
    @IBAction func onTapUpdateButton() {
        self.dataModel.tabName = self.tabNameTextField.text!
        switch CountModeType(rawValue: self.countModePickerView.selectedRow()) {
        case .COUNT:
            self.dataModel.isTimeMode = false
            self.dataModel.isUpdateMode = false
        case .TIME:
            self.dataModel.isTimeMode = true
            self.dataModel.isUpdateMode = false
        case .UPDATE:
            self.dataModel.isTimeMode = false
            self.dataModel.isUpdateMode = true
        default: break
        }
        switch CulcModeType(rawValue: self.culcModePickerView.selectedRow()) {
        case .TOTAL:
            self.dataModel.isCulcAvg = false
        case .AVG:
            self.dataModel.isCulcAvg = true
        default: break
        }
        self.dataModel.backColor = self.colors[self.backColorPickerView.selectedRow()]
        self.dataModel.saveVariable()
        self.delegate.tabSettingUpdate()
        self.removeFromSuperview()
    }
    
    @IBAction func onTapCancelButton() {
        self.removeFromSuperview()
    }
    
    // functions

    func endAllInputing() {
        self.culcModePickerView.removeFromSuperview()
        self.backColorPickerView.removeFromSuperview()
        self.backColorPickerView.removeFromSuperview()
        self.endEditing(true)
    }

    private func countMode() -> CountModeType {
        if self.dataModel.isTimeMode {
            return CountModeType.TIME
        }
        if self.dataModel.isUpdateMode {
            return CountModeType.UPDATE
        }
        return CountModeType.COUNT
    }

    private func culcMode() -> CulcModeType {
        if self.dataModel.isCulcAvg {
            return CulcModeType.AVG
        }
        return CulcModeType.TOTAL
    }

    private func colorPosition() -> Int {
        return self.colors.firstIndex(of: self.dataModel.backColor) ?? 0
    }

    // MARK: - PickerViewDelegate
    
    func dicidePickerInput(pickerView: PickerView) {
        if pickerView == self.countModePickerView {
            self.countModeLabel.text = self.countModePickerView.selectedText()
        } else if pickerView == self.culcModePickerView {
            self.culcModeLabel.text = self.culcModePickerView.selectedText()
        } else if pickerView == self.backColorPickerView {
            self.backColorLabel.text = self.backColorPickerView.selectedText()
            self.backColorLabel.backgroundColor = backColor(self.colors[self.backColorPickerView.selectedRow()])
        }
    }

}
