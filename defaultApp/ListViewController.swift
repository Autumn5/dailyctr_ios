//
//  ListViewController.swift
//  defaultApp
//
//  Created by fujii on 2020/12/26.
//  Copyright © 2020 fujii. All rights reserved.
//

import UIKit

class ListViewController: UIViewController
    ,UITableViewDelegate, UITableViewDataSource
    ,TablistViewControllerDelegate
    ,UIAdaptivePresentationControllerDelegate
    ,TabUtilityDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tabSegment: UISegmentedControl!
    @IBOutlet weak var tabScrollView: UIScrollView!
    var dataModel = AppDataModel()
    var tabTabUtility: TabUtility!
    var isShowDetail: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabTabUtility = TabUtility(parentViewController: self)
        self.tableView.registerNib(withNibName: String(describing: ItemHeaderTableViewCell.self))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadVariable()
    }

    func presentationControllerWillDismiss(_ presentationController: UIPresentationController) {
        self.loadVariable()
    }

    // MARK: - actions
    
    @IBAction func onSelectedTabSegment(_ segmentedControl: UISegmentedControl) {
        self.dataModel.selectedIndex = segmentedControl.selectedSegmentIndex
        self.tableView.reloadData()
    }

    @objc func onTapNavigationTitleButton() {
        let vc = UIStoryboard(name: "TablistViewController", bundle: nil)
            .instantiateInitialViewController() as! TablistViewController
        vc.delegate = self
        vc.presentationController?.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    // MARK: - tableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if 0 < dataModel.dailyTabListSize() {
            return dataModel.dailyTabListSize() + 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0){
            return tableViewHeaderCell(indexPath)
        }
        return tableViewRowDataCell(row: indexPath.row - 1)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 0){
            self.tableViewHeaderDidSelect()
        }
    }
    
    // MARK: tableView - edit
    
    func tableView(_ _: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if (indexPath.row == 0){
            return nil
        }
        return tableViewRowDataTrailingSwipeActions(indexPath.row - 1)
    }
    
    // MARK: - tableView - header
    
    func tableViewHeaderCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: String(describing: ItemHeaderTableViewCell.self), for: indexPath)
                as! ItemHeaderTableViewCell
        cell.setupView(dataModel: self.dataModel, isShowDetail: self.isShowDetail)
        return cell
    }
    
    func tableViewHeaderDidSelect() {
        self.isShowDetail = !self.isShowDetail
        self.tableView.reloadData()
    }

    // MARK: - tableView - rowData
    
    func tableViewRowDataCell(row: Int) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.selectionStyle = .none
        
        let idx = self.dataModel.dailyTabListSize() - row - 1
        cell.textLabel?.text = self.dataModel.dailyTabList(idx).strDate +
            " " + self.dataModel.dailyTabListCountText(idx)
        return cell
    }

    func tableViewRowDataTrailingSwipeActions(_ row: Int) -> UISwipeActionsConfiguration? {
        return UISwipeActionsConfiguration(actions: [self.rowDataDeleteAction(row)])
    }

    func rowDataDeleteAction(_ row: Int) -> UIContextualAction {
        let action = UIContextualAction(style:.normal, title: "削除") { (_, _, handler :(Bool) -> Void) in
            let idx = self.dataModel.dailyTabListSize() - row - 1
            self.dataModel.dailyTabListRemoveAt(idx)
            self.dataModel.saveVariable()
            self.tableView.reloadData()
            handler(true)
        }
        action.backgroundColor = .red
        return action
    }
    
    // functions
    
    func updateTabs() {
        self.tabTabUtility.updateTabs(dataModel: dataModel)
        self.tabTabUtility.setNavigationTitleButton(title: self.dataModel.tabName + "▼",
                                                    action: #selector(self.onTapNavigationTitleButton))
    }

    // functions - TablistViewController

    func tablistViewControllerUpdate() {
        self.loadVariable()
    }

    // MARK: - override AppDataModel

    func loadVariable() {
        self.dataModel.loadVariable()
        self.updateTabs()
        self.tableView.reloadData()
    }

}
