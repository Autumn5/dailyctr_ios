//
//  FileViewController.swift
//  defaultApp
//
//  Created by fujii on 2021/01/09.
//  Copyright © 2020 fujii. All rights reserved.
//

import UIKit

protocol FileViewControllerDelegate : NSObject {
    func selectedFileName(_ fileName : String)
}

class FileViewController: UIViewController
    ,UITableViewDelegate, UITableViewDataSource
{
    public weak var delegate: FileViewControllerDelegate?
    var fileNames: [String]!
    
    // MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.updateFileList()
    }

    override func viewWillDisappear(_ animated: Bool) {
         super.viewWillDisappear(animated)
         presentingViewController?.beginAppearanceTransition(true, animated: animated)
     }

    override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
         presentingViewController?.endAppearanceTransition()
     }
    
    // MARK: - IBAction

    @IBAction func onTapCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - tableview
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fileNames!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FileTableViewCell", for: indexPath)
        cell.textLabel?.text = self.fileNames[indexPath.row]
        let isCSVFile = self.fileNames[indexPath.row].lowercased().hasSuffix(".csv")
        cell.textLabel?.textColor = isCSVFile ? UIColor.label : UIColor.systemGray
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let isCSVFile = self.fileNames[indexPath.row].lowercased().hasSuffix(".csv")
        if isCSVFile {
            self.dismiss(animated: true, completion: nil)
            self.delegate?.selectedFileName(self.fileNames[indexPath.row])
        }
    }

    // MARK: - utility

    private func updateFileList() {
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        if let contentUrls = try? FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: nil) {
            self.fileNames = contentUrls.map{$0.lastPathComponent}
        }
        self.tableView.reloadData()
    }

}
