//
//  GraphView.swift
//  dCounterApp
//
//  Created by fujii on 2021/01/23.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

class GraphView: UIView {
    private var graphArray = Array<GraphPoint>()

    override func draw(_ rect: CGRect) {
        
        self.drawLine(0, 0, 0, self.height, lineWidth: 1)
        self.drawLine(0, self.height, self.width, self.height, lineWidth: 1)

        if 0 < graphArray.count {
            let minCount = CGFloat(GraphModel.minCount(graphArray))
            let heightRate = 1.0 / (CGFloat(GraphModel.maxCount(graphArray)) - minCount)
            let widthRate = 1.0 / (graphArray.last!.x)

            let lineWidth: CGFloat = 2.0
            let width = self.width - lineWidth
            let height = self.height - lineWidth

            func countX(_ x: CGFloat) -> CGFloat {
                return width * x * widthRate + lineWidth / 2
            }
            func countY(_ y: CGFloat) -> CGFloat {
                return height * (1 - (y - minCount) * heightRate) + lineWidth / 2
            }

            for i in 0 ..< graphArray.count {
                self.drawCircle(countX(graphArray[i].x), countY(graphArray[i].y))
                if i < graphArray.count - 1 {
                    self.drawLine(
                            countX(graphArray[i].x), countY(graphArray[i].y),
                            countX(graphArray[i+1].x), countY(graphArray[i+1].y),
                            lineWidth: lineWidth)
                }
            }
        }
    }
    
    // MARK: - functions
    
    func drawCircle(_ x: CGFloat, _ y: CGFloat) {
        let circle = UIBezierPath(arcCenter: CGPoint(x: x, y: y),
                                  radius: 2, startAngle: 0,
                                  endAngle: CGFloat(Double.pi) * 2, clockwise: true)
        UIColor.blue.setFill()
        circle.fill()
    }

    func drawLine(_ x: CGFloat, _ y: CGFloat, _ x2: CGFloat, _ y2: CGFloat, lineWidth: CGFloat) {
        let line = UIBezierPath()
        line.move(to: CGPoint(x: x, y: y))
        line.addLine(to: CGPoint(x: x2, y: y2))
        line.close()
        UIColor.blue.setStroke()
        line.lineWidth = lineWidth
        line.stroke()
    }

    public func setGraphArray(_ array: Array<GraphPoint>) {
        self.graphArray = array
    }

}
