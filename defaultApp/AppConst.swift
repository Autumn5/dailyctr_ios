//
//  AppConst.swift
//  defaultApp
//
//  Created by fujii on 2020/12/28.
//  Copyright © 2020 fujii. All rights reserved.
//

import Foundation

let DATA_FILE_NAME = "countData.plist"
let MAX_TABS = 30
let MAX_SHOW_TABS = 10
let DEFAULT_TABS = 3
let MAX_QR_SIZE = 1024
