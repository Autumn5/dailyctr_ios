//
//  NoSwipeSegmentedControl.swift
//  dCounterApp
//
//  Created by fujii on 2021/01/24.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

class NoSwipeSegmentedControl: UISegmentedControl {
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
