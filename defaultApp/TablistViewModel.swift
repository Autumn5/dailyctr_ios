//
//  TablistViewModel.swift
//  dCounterApp
//
//  Created by fujii on 2021/03/05.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

class TablistViewModel {

    // 指定タブの先頭行のカウント文字列を返す
    class func firstRowCountText(tab: TabData, dataModel: AppDataModel)-> String {
        var count = 0.0
        if 0 < tab.dailyList.count {
            count = dataModel.dailyList[tab.dailyList.last!].count
        }

        if tab.isTimeMode {
            return convStrTime(second: Int(count))
        }
        return count.dispString()
    }

    // 指定タブの先頭行の日付文字列を返す
    class func firstRowDateText(tab: TabData, dataModel: AppDataModel)-> String {
        let strToday = stringFromDate(Date())
        var strDate = ""
        if 0 < tab.dailyList.count {
            strDate = dataModel.dailyList[tab.dailyList.last!].strDate
        }
        if strToday == strDate {
            return "今日"
        }
        if (8 <= strDate.count && strToday.prefix(5) == strDate.prefix(5)) {
            return strDate.substring(from: 5)
        }
        return strDate
    }

    // 絞り込みテキストよりタブIndexリストを作成
    class func createRowList(list: Array<String>, filterText: String)-> Array<Int> {
        var rowList = Array<Int>()
        for idx in 0 ..< list.count {
            if (filterText == "" ||
                list[idx].range(of: filterText, options: .caseInsensitive) != nil) {
                rowList.append(idx)
            }
        }
        return rowList
    }

}
