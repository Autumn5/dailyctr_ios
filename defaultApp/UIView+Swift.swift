//
//  UIView+Swift.swift
//  defaultApp
//
//  Created by fujii on 2021/01/11.
//  Copyright © 2020 fujii. All rights reserved.
//

import UIKit

extension UIView {
    
    var height: CGFloat {
        get{
            return self.frame.size.height
        }
        set{
            var rect: CGRect = self.frame
            rect.size.height = newValue
            self.frame = rect
        }
    }

    var width: CGFloat {
        get{
            return self.frame.size.width
        }
        set{
            var rect: CGRect = self.frame
            rect.size.width = newValue
            self.frame = rect
        }
    }

    var left: CGFloat {
        get{
            return self.frame.origin.x
        }
        set{
            var rect: CGRect = self.frame
            rect.origin.x = newValue
            self.frame = rect
        }
    }

    var right: CGFloat {
        get{
            return self.frame.origin.x + self.width
        }
        set{
            var rect: CGRect = self.frame
            rect.origin.x = newValue - self.width
            self.frame = rect
        }
    }

    var top: CGFloat {
        get{
            return self.frame.origin.y
        }
        set{
            var rect: CGRect = self.frame
            rect.origin.y = newValue
            self.frame = rect
        }
    }
    
    var bottom: CGFloat {
        get{
            return self.frame.origin.y + self.height
        }
        set{
            var rect: CGRect = self.frame
            rect.origin.y = newValue - self.height
            self.frame = rect
        }
    }

    // MARK: - view
    
    class func loadView(nibName name: String, owner ownerOrNil: Any?) -> UIView {
        return UINib(nibName: name, bundle: nil).instantiate(withOwner: ownerOrNil, options: nil).first as! UIView
    }
    
    func addSubview(_ view: UIView, constant c: CGFloat) {
        self.addSubview(view)
        view.leftAnchor.constraint(equalTo: self.leftAnchor, constant: c).isActive = true
        view.rightAnchor.constraint(equalTo: self.rightAnchor, constant: c).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor, constant: c).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: c).isActive = true
    }

    var parentViewController: UIViewController? {
        if self.next is UIViewController {
            return self.next as? UIViewController
        }
        if let next = self.next, next is UIView {
            return (self.next as! UIView).parentViewController
        }
        return nil
    }
}
