//
//  TabUtility.swift
//  dCounterApp
//
//  Created by fujii on 2021/03/06.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

protocol TabUtilityDelegate: UIViewController {
    var tabSegment: UISegmentedControl! { get }
    var tabScrollView: UIScrollView! { get }
}

class TabUtility {
    weak var parentViewController: TabUtilityDelegate!
    weak var tabSegment: UISegmentedControl!
    weak var tabScrollView: UIScrollView!

    init(parentViewController: TabUtilityDelegate) {
        self.parentViewController = parentViewController
        self.tabSegment = parentViewController.tabSegment
        self.tabScrollView = parentViewController.tabScrollView
    }
    
    func setNavigationTitleButton(title: String, action: Selector) {
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.sizeToFit()
        button.width += 10
        button.setTitleColor(.label, for: .normal)
        button.addTarget(self.parentViewController, action: action, for: .touchUpInside)
        self.parentViewController.navigationItem.titleView = button
    }

    func updateTabs(dataModel: AppDataModel) {
        var tabNames = Array<String>()
        var selectedIndex = dataModel.selectedIndex
        for i in 0 ..< dataModel.tabCount {
            if i < MAX_SHOW_TABS || i == dataModel.selectedIndex {
                tabNames.append(dataModel.tabNames[i])
                if i == dataModel.selectedIndex {
                    selectedIndex = tabNames.count - 1
                }
            }
        }
        updateSegments(tabSegment, tabNames: tabNames, selectedIndex: selectedIndex)
        
        DispatchQueue.main.async {
            let tabWidth = self.tabSegment.width / CGFloat(self.tabSegment.numberOfSegments)
            let left = tabWidth * CGFloat(self.tabSegment.selectedSegmentIndex)
            let right = tabWidth * CGFloat(self.tabSegment.selectedSegmentIndex + 1) - self.tabScrollView.width
            if left < self.tabScrollView.contentOffset.x {
                self.tabScrollView.contentOffset = CGPoint(x: left, y: 0)
            } else if self.tabScrollView.contentOffset.x < right {
                self.tabScrollView.contentOffset = CGPoint(x: right, y: 0)
            }
        }
    }
    
}
