//
//  Extension+Control.swift
//  dCounterApp
//
//  Created by fujii on 2021/03/03.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

extension UITableView {
    func registerNib(withNibName nibName: String) {
        let nib = UINib.init(nibName: nibName, bundle: nil)
        self.register(nib, forCellReuseIdentifier: nibName)
    }
}

extension UITextField {
    func setLeftImage(_ image: UIImage?, marginLeft: CGFloat) {
        let imageView = UIImageView.init(image: image)
        let view = UIView()
        if let size = imageView.image?.size {
            view.frame = CGRect(x: 0, y: 0, width: size.width + marginLeft, height: size.height)
        }
        imageView.left = marginLeft
        view.addSubview(imageView)
        self.leftViewMode = .always
        self.leftView = view
    }
}
