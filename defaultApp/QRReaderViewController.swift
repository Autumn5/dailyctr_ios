//
//  QRReaderViewController.swift
//  defaultApp
//
//  Created by fujii on 2021/01/11.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit
import AVFoundation

protocol QRReaderViewControllerDelegate : NSObject {
    func qrReadSuccessed()
}

class QRReaderViewController: UIViewController
    , AVCaptureMetadataOutputObjectsDelegate {

    private let session = AVCaptureSession()
    public weak var delegate: QRReaderViewControllerDelegate!
    var dataModel = AppDataModel()

    @IBOutlet weak var customNavigationBar: UINavigationBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataModel.loadVariable()
        self.setupBarcodeReader()
    }
    
    // MARK: - functions

    private func setupBarcodeReader() {
        guard let device: AVCaptureDevice = AVCaptureDevice.DiscoverySession(
            deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .back).devices.first else {return}

        guard let deviceInput = try? AVCaptureDeviceInput(device: device) else {return}
        if !self.session.canAddInput(deviceInput) {
            return
        }
        self.session.addInput(deviceInput)

        let metadataOutput = AVCaptureMetadataOutput()
        if !self.session.canAddOutput(metadataOutput) {
            return
        }
        self.session.addOutput(metadataOutput)

        metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        metadataOutput.metadataObjectTypes = [.qr]

        // 読み取りエリア
        let widthRate: CGFloat = 0.8
        let heightRate: CGFloat = view.width / view.height * widthRate
        let xRate: CGFloat = (1.0 - widthRate) / 2.0
        let yRate: CGFloat = (1.0 - heightRate) / 2.0
        metadataOutput.rectOfInterest = CGRect(x: yRate, y: xRate, width: heightRate, height: widthRate)
        
        // 読み取りエリアフレーム
        let width: CGFloat = view.width * widthRate
        let frameView = UIView(frame: CGRect(x: (view.width - width) / 2,
                                             y: (view.height - width) / 2,
                                             width: width, height: width))
        frameView.layer.borderColor = UIColor.gray.cgColor
        frameView.layer.borderWidth = 2
        frameView.layer.cornerRadius = 2
        view.addSubview(frameView)

        // カメラ映像表示
        let previewLayer = AVCaptureVideoPreviewLayer(session: self.session)
        previewLayer.frame = self.view.bounds
        previewLayer.videoGravity = .resizeAspectFill
        self.view.layer.insertSublayer(previewLayer, at: 0)

        self.session.startRunning()
    }
    
    // MARK: - AVCaptureMetadataOutputObjectsDelegate

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject],
                        from connection: AVCaptureConnection) {
        for metadata in metadataObjects as! [AVMetadataMachineReadableCodeObject] {
            if metadata.stringValue != nil {
                self.session.stopRunning()
                let isSuccess = self.dataModel.importQR(strData: metadata.stringValue!)
                if isSuccess {
                    self.dataModel.saveVariable()
                }

                let alert = UIAlertController(title: "QR読み込み",
                                              message: isSuccess ? "成功しました" : "失敗しました", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler:{
                    (action: UIAlertAction!) -> Void in
                    self.delegate?.qrReadSuccessed()
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                break
            }
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func onTapCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
