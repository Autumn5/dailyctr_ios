//
//  SettingViewController.swift
//  defaultApp
//
//  Created by fujii on 2021/01/09.
//  Copyright © 2020 fujii. All rights reserved.
//

import UIKit
import SafariServices

let COUNT_LOG_FILE_NAME = "counterLog.csv"
let IMAGE_FILE_NAME = "background.jpg"

class SettingViewController: UITableViewController
    , FileViewControllerDelegate
    , UIImagePickerControllerDelegate
    , UINavigationControllerDelegate
{
    var dataModel = AppDataModel()
    @IBOutlet weak var countLogPathLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.countLogPathLabel.text = COUNT_LOG_FILE_NAME
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if cell.reuseIdentifier == "versionInfo" {
            cell.detailTextLabel?.text = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String?
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if cell.reuseIdentifier == "privacypolicy" {
            let vc = SFSafariViewController(url: URL(string: "https://hp.vector.co.jp/authors/VA021917/DailyCTR/PrivacyPolicy.html")!)
            self.parent?.present(vc, animated: true, completion: nil)
        }
    }

    // MARK: - actions

    @IBAction func onClickCountLogSaveButton() {
        self.dataModel.loadVariable()
        saveStringArray(directory: .documentDirectory, fileName: COUNT_LOG_FILE_NAME, array: self.dataModel.exportCSV())
        let alert = simpleAlert(title: "カウント履歴", message:"保存しました")
        present(alert, animated: true, completion: nil)
    }

    @IBAction func onClickImageLoadButton() {
        let pickerView = UIImagePickerController()
        pickerView.sourceType = .photoLibrary
        pickerView.delegate = self
        self.present(pickerView, animated: true)
    }
    
    @IBAction func onClickImageDeleteButton() {
        deleteImage(url: documentURL(IMAGE_FILE_NAME))
        let alert = simpleAlert(title: "背景イメージ", message:"削除しました")
        present(alert, animated: true, completion: nil)
    }

    // MARK: - segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FileViewController" {
            let vc = segue.destination as! FileViewController
            vc.delegate = self
        }
    }

    // MARK: - FileViewController delegate

    func selectedFileName(_ fileName: String) {
        let csvArray = loadStringArray(directory: .documentDirectory, fileName: fileName)
        self.dataModel.loadVariable()
        let isSuccess = self.dataModel.importCSV(csvArray: csvArray)
        if isSuccess {
            self.dataModel.saveVariable()
        }
        let alert = simpleAlert(title: "カウント履歴",
                                        message:isSuccess ? "読み込みました" : "読み込みに失敗しました")
        present(alert, animated: true, completion: nil)
    }

    // MARK: - UIImagePickerControllerDelegate delegate

    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true)
        let image = info[.originalImage] as! UIImage
        let isSuccess = saveImage(image:resize(image: image, maxWidth: 640),
                                  url:documentURL(IMAGE_FILE_NAME))
        let alert = simpleAlert(title: "背景イメージ",
                                message:isSuccess ? "読み込みました" : "読み込みに失敗しました")
        present(alert, animated: true, completion: nil)
    }

}
