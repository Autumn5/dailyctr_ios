//
//  ViewController.swift
//  defaultApp
//
//  Created by fujii on 2020/12/22.
//  Copyright © 2020 fujii. All rights reserved.
//

import UIKit

enum CountTermType : Int {
    case TERM_DAYLY
    case TERM_WEEKLY
    case TERM_MONTHLY
}

class ViewController: UIViewController
    ,QRReaderViewControllerDelegate
    ,HomeTabSettingViewDelegate
    ,TablistViewControllerDelegate
    ,UIAdaptivePresentationControllerDelegate
    ,TabUtilityDelegate
{
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var yesterdayCountLabel: UILabel!
    @IBOutlet weak var totalCountLabel: UILabel!
    @IBOutlet weak var yesterdayTitleLabel: UILabel!
    @IBOutlet weak var totalTitleLabel: UILabel!
    @IBOutlet weak var todayTitleLabel: UILabel!
    @IBOutlet weak var beforeButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var countBackView: UIView!
    @IBOutlet weak var countButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var incrementTextField: UITextField!
    @IBOutlet weak var incrementTitleLabel: UILabel!
    @IBOutlet weak var incrementBackView: UIView!
    @IBOutlet weak var tabSegment: UISegmentedControl!
    @IBOutlet weak var tabScrollView: UIScrollView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var countLabelAndTimeLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var countLabelAndYesterdayCountLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
    var dataModel = AppDataModel()
    var countTerm = CountTermType.TERM_DAYLY
    var isHandlerRunning = false
    var tabTabUtility: TabUtility!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabTabUtility = TabUtility(parentViewController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadVariable()
        self.updateTabAndCount()

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didBecomeActive),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        
        self.imageView.image = loadImage(url: documentURL(IMAGE_FILE_NAME))
        self.imageView.alpha = (self.imageView.image != nil) ? 0.8 : 1
    }

    func presentationControllerWillDismiss(_ presentationController: UIPresentationController) {
        self.loadVariable()
        self.updateTabAndCount()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    @objc func didBecomeActive() {
        self.updateCount()
        self.saveVariable()
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        self.bottomConstraint.constant = keyboardFrame.height - self.view.safeAreaInsets.bottom
        self.view.bringSubviewToFront(self.tabScrollView)
    }

    @objc func keyboardWillHide(_ notification: Notification) {
        self.bottomConstraint.constant = 0
    }

    // MARK: - actions
    
    @IBAction func onTapGesture(_ gesture: UITapGestureRecognizer) {
        let isHidden = !self.beforeButton.isHidden
        self.beforeButton.isHidden = isHidden
        self.nextButton.isHidden = isHidden
        self.yesterdayTitleLabel.isHidden = isHidden
        self.yesterdayCountLabel.isHidden = isHidden
        self.totalTitleLabel.isHidden = isHidden
        self.totalCountLabel.isHidden = isHidden
        self.todayTitleLabel.isHidden = isHidden
        self.timeLabel.isHidden = isHidden
        self.countLabelAndTimeLabelConstraint.priority = UILayoutPriority(rawValue: isHidden ? 1 : 1000)
        self.countLabelAndYesterdayCountLabelConstraint.priority = UILayoutPriority(rawValue: isHidden ? 1 : 751)
        self.setCountLabelText(self.countLabel.text!)
    }
    
    @IBAction func onSelectedTabSegment(_ segmentedControl: UISegmentedControl) {
        self.dataModel.selectedIndex = segmentedControl.selectedSegmentIndex
        if MAX_SHOW_TABS < self.tabSegment.numberOfSegments && dataModel.selectedIndex < MAX_SHOW_TABS {
            updateTabs()
        }
        self.updateTabAndCount()
        self.saveVariable()
    }

    @objc func onTapNavigationTitleButton() {
        let vc = UIStoryboard(name: "TablistViewController", bundle: nil)
            .instantiateInitialViewController() as! TablistViewController
        vc.delegate = self
        vc.presentationController?.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onTapCountButton() {
        self.hideKeyboard()
        self.updateCount(self.getIncrement())
        self.saveVariable()
    }
    
    @IBAction func onTapClearButton() {
        if self.isHandlerRunning {
            self.intervalCancel()
            return
        }
        if 0 < self.dataModel.count {
            self.dataModel.countClear()
            self.updateCount()
            self.saveVariable()
            return
        }

        let alert = UIAlertController(title: "クリア", message: "合計と履歴もクリアしますか？", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "クリア", style: .default, handler:{
            (action: UIAlertAction!) -> Void in
            self.dataModel.countAllClear()
            self.updateCount()
            self.saveVariable()
        })
        alert.addAction(defaultAction)
        alert.addAction(UIAlertAction(title: "キャンセル", style: .cancel, handler: nil))

        present(alert, animated: true, completion: nil)
    }

    @IBAction func onClickStartButton() {
        if self.isHandlerRunning {
            self.intervalStop()
            let increment = HomeViewModel.timeCount(self.dataModel)
            self.dataModel.startDate = nil
            self.updateCount(Double(increment))
            self.saveVariable()
        } else {
            self.dataModel.startDate = Date()
            self.saveVariable()
            self.intervalStart()
        }
    }

    @IBAction func onTapBeforeButton() {
        if self.countTerm == CountTermType.TERM_DAYLY {
            self.countTerm = CountTermType.TERM_MONTHLY
        } else if self.countTerm == CountTermType.TERM_MONTHLY {
            self.countTerm = CountTermType.TERM_WEEKLY
        } else {
            self.countTerm = CountTermType.TERM_DAYLY
        }
        self.updateCount()
    }

    @IBAction func onTapNextButton() {
        if self.countTerm == CountTermType.TERM_DAYLY {
            self.countTerm = CountTermType.TERM_WEEKLY
        } else if self.countTerm == CountTermType.TERM_WEEKLY {
            self.countTerm = CountTermType.TERM_MONTHLY
        } else {
            self.countTerm = CountTermType.TERM_DAYLY
        }
        self.updateCount()
    }

    @IBAction func incrementTextFieldDidEndOnExit() {
    }
    
    @IBAction func editingDidBeginIncrementTextField() {
        var toolBar: UIToolbar? = nil
        if 1 < self.dataModel.incrementHistories.count {
            var texts = [String]()
            for i in 1 ..< self.dataModel.incrementHistories.count {
                texts.append(self.dataModel.incrementHistories[i].dispString())
            }
            toolBar = Utility_toolBar(withButtonTexts:texts,
                target:self, action:#selector(onTapIncrementTextFieldToolBarButton(_:)))
        }
        self.incrementTextField.inputAccessoryView = toolBar
    }

    @objc func onTapIncrementTextFieldToolBarButton(_ button: UIBarButtonItem) {
        self.incrementTextField.text = button.title
    }

    @IBAction func onTapSyncButton() {
        var actions = [UIAlertAction]()
        actions.append(UIAlertAction(title: "タブ追加", style: .default, handler: { _ in
            self.dataModel.insertTab()
            self.dataModel.saveVariable()
            self.loadVariable()
            self.updateTabAndCount()
            self.showTabSettingDialog()
        }))
        if MAX_TABS <= self.dataModel.tabCount {
            actions.last!.isEnabled = false
        }
        actions.append(UIAlertAction(title: "タブ削除", style: .default, handler: { _ in
            let deleteTabBlock = {
                self.dataModel.removeTabAndList()
                self.dataModel.saveVariable()
                self.loadVariable()
                self.updateTabAndCount()
            }
            if self.dataModel.isClearTabAndList {
                deleteTabBlock()
            } else {
                let alert = UIAlertController(title: "タブ削除", message: "現在のタブを削除しますか？", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "削除", style: .default, handler:{
                    (action: UIAlertAction!) -> Void in
                    deleteTabBlock()
                }))
                alert.addAction(UIAlertAction(title: "キャンセル", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }))
        actions.append(UIAlertAction(title: "タブ設定", style: .default, handler: { _ in
            self.showTabSettingDialog()
        }))
        if self.dataModel.startDate != nil {
            actions.last!.isEnabled = false
        }
        actions.append(UIAlertAction(title: "QRコード表示", style: .default, handler: { _ in
            let vc = UIStoryboard(name: "QRDisplayViewController", bundle: nil)
                .instantiateInitialViewController() as! QRDisplayViewController
            self.present(vc, animated: true, completion: nil)
        }))
        actions.append(UIAlertAction(title: "QRコード読込", style: .default, handler: { _ in
            let vc = UIStoryboard(name: "QRReaderViewController", bundle: nil)
                .instantiateInitialViewController() as! QRReaderViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }))
        actions.append(UIAlertAction(title: "グラフ表示", style: .default, handler: { _ in
            let vc = UIStoryboard(name: "GraphViewController", bundle: nil)
                .instantiateInitialViewController() as! GraphViewController
            vc.countTerm = self.countTerm
            self.present(vc, animated: true, completion: nil)
        }))
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for action in actions {alert.addAction(action)}
        alert.addAction(UIAlertAction(title: "キャンセル", style: .cancel))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - QRReaderViewControllerDelegate
    
    func qrReadSuccessed() {
        self.loadVariable()
        self.updateCount()
        self.saveVariable()
    }
    
    // MARK: - functions

    func getIncrement() -> Double {
        var increment = 1.0
        if let newIncrement = Double(self.incrementTextField.text!)?.fix() {
            increment = newIncrement
        }
        self.incrementTextField.text = increment.dispString()
        return increment
    }

    func setCountLabelText(_ text: String) {
        if self.beforeButton.isHidden == false {
            self.countLabel.font = UIFont.systemFont(ofSize: 8 <= text.count ? 25 : 40)
        } else {
            self.countLabel.font = UIFont.systemFont(ofSize: 60)
        }
        self.countLabel.text = text
    }

    func updateTabs() {
        self.tabTabUtility.updateTabs(dataModel: dataModel)
    }

    func updateCountMode() {
        if self.dataModel.isTimeMode {
            self.countButton.isHidden = true
            self.startButton.isHidden = false
            self.incrementTextField.isHidden = true
            self.incrementTitleLabel.isHidden = true
            self.incrementBackView.isHidden = true

            if self.dataModel.startDate != nil {
                self.intervalStart()
            } else {
                self.intervalStop()
            }
        } else {
            self.countButton.isHidden = false
            self.startButton.isHidden = true
            self.incrementTextField.isHidden = false
            self.incrementTitleLabel.isHidden = false
            self.incrementBackView.isHidden = false
            self.intervalCancel()

            if self.dataModel.isUpdateMode {
                self.countButton.setTitle(string_title_update, for: .normal)
            } else {
                self.countButton.setTitle(string_title_count, for: .normal)
            }
        }
    }

    // functions - tabSettingDialog

    func showTabSettingDialog() {
        let view = HomeTabSettingView.instance(delegate: self, dataModel: self.dataModel)
        self.tabBarController!.view.addSubview(view, constant: 0)
    }

    func tabSettingUpdate() {
        self.loadVariable()
        self.updateTabAndCount()
    }

    // functions - TablistViewController

    func tablistViewControllerUpdate() {
        self.loadVariable()
        self.updateTabAndCount()
    }

    // MARK: - functions - interval

    func intervalStart() {
        self.isHandlerRunning = true
        self.startButton.setTitle("ストップ", for: .normal)
        self.interval()
    }

    func intervalStop() {
        self.isHandlerRunning = false
        self.startButton.setTitle("スタート", for: .normal)
    }

    func intervalCancel() {
        if self.isHandlerRunning {
            self.dataModel.startDate = nil
            self.saveVariable()
            self.intervalStop()
            self.updateCount()
        }
    }

    func interval() {
        if self.isHandlerRunning {
            self.updateCount()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.interval()
            }
        }
    }

    // MARK: - functions - override AppDataModel

    func updateTabAndCount() {
        self.hideKeyboard()
        self.updateCount()
        self.incrementTextField.text = self.dataModel.increment.dispString()
        self.imageView.backgroundColor = backColor(self.dataModel.backColor)
        self.countBackView.backgroundColor = backColor(self.dataModel.backColor)
        self.incrementBackView.backgroundColor = backColor(self.dataModel.backColor)
        self.updateCountMode()
        
        self.tabTabUtility.setNavigationTitleButton(title: self.dataModel.tabName + "▼",
                                                    action: #selector(self.onTapNavigationTitleButton))
    }

    func updateCount(_ increment: Double? = nil) {
        if increment != nil {
            self.dataModel.updateCount(increment: increment!)
        } else {
            self.dataModel.updateCount()
        }
        self.setCountLabelText(HomeViewModel.todayCountText(self.dataModel))
        self.timeLabel.text = HomeViewModel.strTime(self.dataModel)
        self.yesterdayTitleLabel.text = HomeViewModel.yesterdayTitle(self.countTerm)
        self.totalTitleLabel.text = HomeViewModel.totalTitle(self.dataModel, self.countTerm)

        let sumCountTexts = HomeViewModel.sumCountTexts(self.dataModel, self.countTerm)
        self.yesterdayCountLabel.text = sumCountTexts[0]
        self.totalCountLabel.text = sumCountTexts[1]
    }

    func loadVariable() {
        self.dataModel.loadVariable()
        self.updateTabs()
    }

    func saveVariable() {
        self.dataModel.increment = self.getIncrement()
        self.dataModel.saveVariable()
    }

    // MARK: - Utility methods

    func hideKeyboard() {
        self.view.endEditing(true)
    }
}
