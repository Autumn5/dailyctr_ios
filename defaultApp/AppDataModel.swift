//
//  MenuListModel.swift
//  defaultApp
//
//  Created by fujii on 2020/12/28.
//  Copyright © 2020 fujii. All rights reserved.
//

import UIKit

struct TabData {
    var count: Double = 0
    var yesterDayCount: Double = 0
    var totalCount: Double = 0
    var strDate: String = ""
    var strTime: String = "-"
    var increment: Double = 1
    var incrementHistories: Array<Double> = Array<Double>()
    var tabName: String = ""
    var isTimeMode: Bool = false
    var isCulcAvg: Bool = false
    var isUpdateMode: Bool = false
    var startDate: Date? = nil
    var backColor: Int = 0
    var dailyList: Array<Int> = Array<Int>()
   
    // カウント表示文字列を返す
    func countText() -> String {
        if self.isTimeMode {
            return convStrTime(second: Int(self.count))
        }
        return self.count.dispString()
    }

}

struct DailyData {
    var count: Double = 0
    var strDate: String = ""
    var tab: Int = 0
}

class AppDataModel: NSObject {
   var selectedIndex = 0
   var tabCount = DEFAULT_TABS
   var tabDatas = Array<TabData>()
   var dailyList = Array<DailyData>()

    var count: Double {
        get { return self.tabDatas[selectedIndex].count }
        set { self.tabDatas[selectedIndex].count = newValue }
    }
    var yesterDayCount: Double {
        get { return self.tabDatas[selectedIndex].yesterDayCount }
        set { self.tabDatas[selectedIndex].yesterDayCount = newValue }
    }
    var totalCount: Double {
        get { return self.tabDatas[selectedIndex].totalCount }
        set { self.tabDatas[selectedIndex].totalCount = newValue }
    }
    var strDate: String {
        get { return self.tabDatas[selectedIndex].strDate }
        set { self.tabDatas[selectedIndex].strDate = newValue }
    }
    var strTime: String {
        get { return self.tabDatas[selectedIndex].strTime }
        set { self.tabDatas[selectedIndex].strTime = newValue }
    }
    var increment: Double {
        get { return self.tabDatas[selectedIndex].increment }
        set { self.tabDatas[selectedIndex].increment = newValue }
    }
    var incrementHistories: Array<Double> {
        get { return self.tabDatas[selectedIndex].incrementHistories }
        set { self.tabDatas[selectedIndex].incrementHistories = newValue }
    }
    var tabName: String {
        get { return self.tabDatas[selectedIndex].tabName }
        set { self.tabDatas[selectedIndex].tabName = newValue }
    }
    var isTimeMode: Bool {
        get { return self.tabDatas[selectedIndex].isTimeMode }
        set { self.tabDatas[selectedIndex].isTimeMode = newValue }
    }
    var isCulcAvg: Bool {
        get { return self.tabDatas[selectedIndex].isCulcAvg }
        set { self.tabDatas[selectedIndex].isCulcAvg = newValue }
    }
    var isUpdateMode: Bool {
        get { return self.tabDatas[selectedIndex].isUpdateMode }
        set { self.tabDatas[selectedIndex].isUpdateMode = newValue }
    }
    var startDate: Date? {
        get { return self.tabDatas[selectedIndex].startDate }
        set { self.tabDatas[selectedIndex].startDate = newValue }
    }
    var backColor: Int {
        get { return self.tabDatas[selectedIndex].backColor }
        set { self.tabDatas[selectedIndex].backColor = newValue }
    }
    var tabNames: Array<String> {
        get { return self.tabDatas.map { $0.tabName } }
    }
    var isClearTabAndList: Bool {
        get { return (self.count == 0 &&
                    self.totalCount == 0 &&
                    self.dailyTabListSize() == 0) }
    }
    var dailyCountList: Array<Double> {
        get { return self.dailyList.map { $0.count } }
    }
    var dailyDateList: Array<String> {
        get { return self.dailyList.map { $0.strDate } }
    }
    var dailyTabList: Array<Int> {
        get { return self.dailyList.map { $0.tab } }
    }

    override init() {
        for _ in 0 ..< MAX_TABS {
            self.tabDatas.append(TabData())
        }
    }

    // MARK: - functions

    func updateCount() {
        let strNewDate = stringFromDate(Date())
        if strNewDate != self.strDate {
            let strNextDate = strDateAddDays(self.strDate, days: 1)
            if strNextDate != nil && strNewDate == strNextDate {
                self.yesterDayCount = self.count
            } else {
                self.yesterDayCount = 0.0
            }

            self.count = 0.0
            self.strDate = strNewDate
            self.strTime = "-"
        }
    }

    func updateCount(increment: Double) {
        self.updateCount()

        if self.isUpdateMode {
            self.totalCount += -self.count + increment
            self.count = increment
        } else {
            self.count += increment
            self.totalCount += increment
        }

        if self.incrementHistories.contains(increment) {
            self.incrementHistories.removeAll(where:{ $0 == increment })
        }
        self.incrementHistories.insert(increment, at: 0)
        if 4 < self.incrementHistories.count {
            self.incrementHistories.removeLast()
        }
        self.strTime = stringFromDate(Date(), format: "HH:mm")

        self.addOrUpdateDailyCount(self.strDate, self.count)
    }

    func updateCount(tabIndex: Int, increment: Double) {
        let oldSelectedCount = self.selectedIndex
        self.selectedIndex = tabIndex
        self.updateCount(increment: increment)
        self.selectedIndex = oldSelectedCount
    }

    func addOrUpdateDailyCount(_ strDate: String, _ count: Double) {
        var isMatch = false
        for i in 0 ..< self.dailyList.count {
            if self.dailyList[i].tab == self.selectedIndex
                    && self.dailyList[i].strDate == strDate {
                self.dailyList[i].count = count
                isMatch = true
                break
            }
        }
        if !isMatch {
            self.dailyList.append(
                DailyData(count: count, strDate: strDate, tab: self.selectedIndex))
            self.tabDatas[self.selectedIndex].dailyList.append(self.dailyList.count - 1)
        }
    }

    func countClear() {
        self.totalCount -= self.count
        self.count = 0
        self.strTime = "-"
        self.addOrUpdateDailyCount(self.strDate, self.count)
    }

    func countAllClear() {
        self.yesterDayCount = 0
        self.totalCount = 0
        self.clearAllDailyCount(self.selectedIndex)
        self.updateDailyTabLists()
    }

    func dailyTabList(_ idx: Int)-> DailyData {
        return self.dailyList[self.tabDatas[self.selectedIndex].dailyList[idx]]
    }

    func dailyTabListCountText(_ idx: Int)-> String {
        let count = self.dailyList[self.tabDatas[self.selectedIndex].dailyList[idx]].count
        if self.isTimeMode {
            return convStrTime(second: Int(count))
        }
        return count.dispString()
    }
    
    func dailyCountTextList()-> Array<String> {
        var texts = Array<String>()
        for i in 0 ..< self.dailyList.count {
            let count = self.dailyList[i].count
            if self.tabDatas[self.dailyList[i].tab].isTimeMode {
                texts.append(convStrTime(second: Int(count)))
            } else {
                texts.append(count.dispString())
            }
        }
        return texts
    }

    func dailyTabListSize()-> Int {
        return self.tabDatas[self.selectedIndex].dailyList.count
    }

    func dailyTabListRemoveAt(_ idx: Int) {
        let orgIdx = self.tabDatas[self.selectedIndex].dailyList[idx]
        self.dailyList.remove(at: orgIdx)
        self.updateDailyTabLists()
    }

    private func updateDailyTabLists() {
        for i in 0 ..< MAX_TABS {
            self.tabDatas[i].dailyList.removeAll()
        }
        for idx in 0 ..< self.dailyList.count {
            if 0 <= self.dailyList[idx].tab && self.dailyList[idx].tab < self.tabDatas.count {
                self.tabDatas[self.dailyList[idx].tab].dailyList.append(idx)
            }
        }
    }

    //MARK: functions - 集計

    // 全期間のカウント数を返す(合計 or 平均)
    func countOfAll()-> Double {
        if self.isCulcAvg {
            return averageOfAll()
        }
        return self.totalCount
    }

    // 全期間の平均カウント数を返す
    func averageOfAll()-> Double {
        if (0 < self.dailyTabListSize()) {
            var totalCount = 0.0
            for idx in 0 ..< self.dailyTabListSize() {
                totalCount += self.dailyTabList(idx).count
            }
            return totalCount / Double(self.dailyTabListSize())
        }
        return 0.0
    }

    // 全期間の最小カウント数を返す
    func minOfAll()-> Double {
        if (0 < self.dailyTabListSize()) {
            var minCount = Double.greatestFiniteMagnitude
            for idx in 0 ..< self.dailyTabListSize() {
                minCount = min(minCount, self.dailyTabList(idx).count)
            }
            return minCount
        }
        return 0.0
    }

    // 日付情報(開始日・終了日・期間)を返す
    func dateInfoOfAll() -> DateInfo {
        let info = DateInfo()
        if (dailyTabListSize() == 0) {
            return info
        }

        var minDate: Date! = nil
        var maxDate: Date! = nil
        for i in 0 ..< dailyTabListSize() {
            if let date = dateFromString(dailyTabList(i).strDate) {
                if (minDate == nil || date.compare(minDate) == .orderedAscending) {
                    minDate = date
                }
                if (maxDate == nil || date.compare(maxDate) == .orderedDescending) {
                    maxDate = date
                }
            }
        }
        if minDate != nil {
            info.minStrDate = stringFromDate(minDate!)
            info.maxStrDate = stringFromDate(maxDate!)
            info.diffDays = diffDays(minDate!, maxDate!) + 1
        }
        return info
    }

    // 日付情報(開始日・終了日・期間)
    class DateInfo {
        var minStrDate: String = ""
        var maxStrDate: String = ""
        var diffDays: Int = 0
    }

    // 全期間の最大カウント数を返す
    func maxOfAll()-> Double {
        var minCount = 0.0
        for idx in 0 ..< self.dailyTabListSize() {
            minCount = max(minCount, self.dailyTabList(idx).count)
        }
        return minCount
    }

    // 今月・先月のカウント数を返す(合計 or 平均)
    func countOfMonth()-> (Double, Double)  {
        let thisMonth = dateSet(today(), day: 1)
        let nextMonth = dateAddMonths(thisMonth, months: 1)
        let beforeMonth = dateAddMonths(thisMonth, months: -1)
        var thisCount = 0.0
        var thisTimes = 0
        var beforeCount = 0.0
        var beforeTimes = 0
        for idx in 0 ..< self.dailyTabListSize() {
            let date = dateFromString(self.dailyTabList(idx).strDate)!
            if date.compare(thisMonth) != .orderedAscending && date.compare(nextMonth) == .orderedAscending {
                thisCount += self.dailyTabList(idx).count
                thisTimes += 1
            }
            if date.compare(beforeMonth) != .orderedAscending && date.compare(thisMonth) == .orderedAscending {
                beforeCount += self.dailyTabList(idx).count
                beforeTimes += 1
            }
        }
        // 平均
        if self.isCulcAvg {
            if 0 < thisTimes {
                thisCount /= Double(thisTimes)
            }
            if 0 < beforeTimes {
                beforeCount /= Double(beforeTimes)
            }
        }
        return (thisCount, beforeCount)
    }

    // 今週・先週のカウント数を返す(合計 or 平均)
    func countOfWeek()-> (Double, Double)  {
        let thisWeek = dateAddDays(today(), days: -week(Date()))
        let nextWeek = dateAddDays(thisWeek, days: 7)
        let beforeWeek = dateAddDays(thisWeek,days: -7)
        var thisCount = 0.0
        var thisTimes = 0
        var beforeCount = 0.0
        var beforeTimes = 0
        for idx in 0 ..< self.dailyTabListSize() {
            let date = dateFromString(self.dailyTabList(idx).strDate)!
            if date.compare(thisWeek) != .orderedAscending && date.compare(nextWeek) == .orderedAscending {
                thisCount += self.dailyTabList(idx).count
                thisTimes += 1
            }
            if date.compare(beforeWeek) != .orderedAscending && date.compare(thisWeek) == .orderedAscending {
                beforeCount += self.dailyTabList(idx).count
                beforeTimes += 1
            }
        }
        // 平均
        if self.isCulcAvg {
            if 0 < thisTimes {
                thisCount /= Double(thisTimes)
            }
            if 0 < beforeTimes {
                beforeCount /= Double(beforeTimes)
            }
        }
        return (thisCount, beforeCount)
    }

    // 月単位のカウント数を返す(合計 or 平均)
    func countsByMonth() -> (Array<Date>, Array<Double>)  {
        var dateArray = Array<Date>()
        var countArray = Array<Double>()
        var timesArray = Array<Int>()
        for idx in 0 ..< self.dailyTabListSize() {
            let monthDate = dateSet(dateFromString(self.dailyTabList(idx).strDate)!, day: 1)
            let idxDate = dateArray.firstIndex(of: monthDate) ?? -1
            if 0 <= idxDate {
                countArray[idxDate] += self.dailyTabList(idx).count
                timesArray[idxDate] += 1
            } else {
                dateArray.append(monthDate)
                countArray.append(self.dailyTabList(idx).count)
                timesArray.append(1)
            }
        }
        // 平均
        if self.isCulcAvg {
            for idx in 0 ..< countArray.count {
                if 0 < timesArray[idx] {
                    countArray[idx] /= Double(timesArray[idx])
                }
            }
        }
        return (dateArray, countArray)
    }

    // 週単位のカウント数を返す(合計 or 平均)
    func countsByWeek() -> (Array<Date>, Array<Double>)  {
        var dateArray = Array<Date>()
        var countArray = Array<Double>()
        var timesArray = Array<Int>()
        for idx in 0 ..< self.dailyTabListSize() {
            let date = dateFromString(self.dailyTabList(idx).strDate)!
            let weekDate = dateAddDays(date, days: -week(date))
            let idxDate = dateArray.firstIndex(of: weekDate) ?? -1
            if 0 <= idxDate {
                countArray[idxDate] += self.dailyTabList(idx).count
                timesArray[idxDate] += 1
            } else {
                dateArray.append(weekDate)
                countArray.append(self.dailyTabList(idx).count)
                timesArray.append(1)
            }
        }
        // 平均
        if self.isCulcAvg {
            for idx in 0 ..< countArray.count {
                if 0 < timesArray[idx] {
                    countArray[idx] /= Double(timesArray[idx])
                }
            }
        }
        return (dateArray, countArray)
    }

    // MARK: - functions - QRコード

    func createQRData() -> (qrString:String, count:Int) {
        var strData = self.strDate + "," +
                self.strTime + "," +
                self.count.dispString() + "," +
                self.yesterDayCount.dispString() + "," +
                self.totalCount.dispString() + ",{"

        let mIdx = self.dailyTabListSize() - 1
        var lines = Array<String>()
        var length = strData.count
        for idx in 0 ..< mIdx + 1 {
            let line = self.dailyTabList(mIdx - idx).strDate + "," +
                    self.dailyTabList(mIdx - idx).count.dispString()
            if MAX_QR_SIZE < length + line.count + 1 {
                break
            }
            lines.insert(line, at: 0)
            length += line.count + 1
        }
        strData += lines.joined(separator: ",") + "}"
        return (strData, lines.count)
    }

    func importQR(strData: String) -> Bool {
        let array = strData.split(separator: ",").map{ String($0) }
        if array.count < 6 {
            return false
        }

        var isSuccess = false
        self.strDate = array[0]
        self.strTime = array[1]
        self.count = Double(array[2]) ?? 0
        self.yesterDayCount = Double(array[3]) ?? 0
        self.totalCount = Double(array[4]) ?? 0

        if array[5] == "{}" {
            return true
        }

        for i in 0 ..< ((array.count - 5) / 2) {
            let pIdx = i * 2 + 5

            var strDate = array[pIdx]
            if i == 0 {
                if (strDate.hasPrefix("{")) {
                    strDate = strDate.substring(from: 1)
                } else {
                    break
                }
            }
            var strCount = array[pIdx + 1]
            if (strCount.hasSuffix("}")) {
                strCount = strCount.substring(to: strCount.count - 1)
                isSuccess = true
            }
            self.addOrUpdateDailyCount(strDate, Double(strCount) ?? 0)
        }
        return isSuccess
    }

    // MARK: - functions - タブ追加/削除
    
    func insertTab() {
        if MAX_TABS <= self.tabCount {
            return
        }
        self.selectedIndex += 1
        if self.selectedIndex < self.tabCount {
            for i in (self.selectedIndex + 1 ... MAX_TABS - 1).reversed() {
                self.copyTab(fromIndex: i - 1, toIndex: i)
                self.moveAllDailyCount(fromIndex: i - 1, toIndex: i)
            }
        }
        self.clearTab(self.selectedIndex)
        self.updateDailyTabLists()
        self.tabCount += 1
    }

    func moveTab(fromIndex: Int, toIndex: Int) {
        self.moveAllDailyCount(fromIndex: fromIndex, toIndex: Int.max)
        let tab = self.tabDatas[fromIndex]

        if fromIndex < toIndex {
            for i in fromIndex ..< toIndex {
                self.copyTab(fromIndex: i + 1, toIndex: i)
                self.moveAllDailyCount(fromIndex: i + 1, toIndex: i)
            }
        } else {
            for i in (toIndex + 1 ... fromIndex).reversed() {
                self.copyTab(fromIndex: i - 1, toIndex: i)
                self.moveAllDailyCount(fromIndex: i - 1, toIndex: i)
            }
        }

        self.tabDatas[toIndex] = tab
        self.moveAllDailyCount(fromIndex: Int.max, toIndex: toIndex)
        self.updateDailyTabLists()
    }

    func removeTabAndList() {
        if self.tabCount <= 1 {
            clearTabAndList()
            return
        }
        self.clearAllDailyCount(self.selectedIndex)
        for i in self.selectedIndex ..< MAX_TABS - 1 {
            self.copyTab(fromIndex: i + 1, toIndex: i)
            self.moveAllDailyCount(fromIndex: i + 1, toIndex: i)
        }
        self.clearTab(MAX_TABS - 1)
        self.updateDailyTabLists()
        self.tabCount -= 1
        if self.tabCount <= self.selectedIndex {
            self.selectedIndex = self.tabCount - 1
        }
    }

    private func clearTabAndList() {
        self.clearAllDailyCount(self.selectedIndex)
        self.clearTab(self.selectedIndex)
        self.updateDailyTabLists()
    }

    private func copyTab(fromIndex: Int, toIndex: Int) {
        self.tabDatas[toIndex] = self.tabDatas[fromIndex]
    }

    private func clearTab(_ index: Int) {
        self.tabDatas[index] = TabData(tabName: (index + 1).description)
    }

    // 要updateDailyTabLists
    private func moveAllDailyCount(fromIndex: Int, toIndex: Int) {
        for i in 0 ..< self.dailyList.count {
            if self.dailyList[i].tab == fromIndex {
                self.dailyList[i].tab = toIndex
            }
        }
    }

    // 要updateDailyTabLists
    private func clearAllDailyCount(_ index: Int) {
        var pointer = 0
        for _ in 0 ..< self.dailyList.count {
            if self.dailyList[pointer].tab == index {
                self.dailyList.remove(at: pointer)
            } else {
                pointer += 1
            }
        }
    }

    // MARK: - functions - CSV読込

    func importCSV(csvArray : Array<String>) -> Bool {
        let arrays = stringArrays(csvArray: csvArray)
        if arrays.count >= 2 {
            self.dailyList.removeAll()
            for idx in 0 ..< arrays[0].count {
                let strDate = strDateAddDays(arrays[0][idx], days: 0) ?? arrays[0][idx]
                let count = convDouble(strCount: arrays[1][idx])
                self.dailyList.append(DailyData(count: count, strDate: strDate))
            }
            if arrays.count >= 3 {
                self.replaceTabs(arrays[2])
                self.tabCount = max(arrays[2].distinct().count, DEFAULT_TABS)
            } else {
                self.replaceTabs(stringList(count: self.dailyList.count, value: "1"))
                self.tabCount = DEFAULT_TABS
            }
            self.updateAllTabCountsFromHistory()
            self.updateTimeMode(arrays[1])
            return true
        }
        return false
    }

    // 履歴のタブインデックスと各タブ名を置き換える
    // 既存に存在しないタブは情報を初期化する
    private func replaceTabs(_ dailyTabNameList: Array<String>) {
        let newTabNames = dailyTabNameList.distinct()
        let newTabCount = newTabNames.count
        var newDiffTabNames = Array<String>()
        for newTabName in newTabNames {
            let index = self.tabNames.firstIndex(of: newTabName) ?? -1
            if (0 <= index && index < newTabCount) {
                continue
            }
            newDiffTabNames.append(newTabName)
        }
        for i in 0 ..< MAX_TABS {
            if (!newTabNames.contains(self.tabDatas[i].tabName)) {
                self.clearTab(i)
                if 0 < newDiffTabNames.count {
                    self.tabDatas[i].tabName = newDiffTabNames.first!
                    newDiffTabNames.removeFirst()
                }
            }
        }
        let dailyTabList = indexList(nameList: dailyTabNameList, nameMasterList: self.tabNames)
        for idx in 0 ..< min(dailyTabList.count, self.dailyList.count) {
            self.dailyList[idx].tab = dailyTabList[idx]
        }
        self.updateDailyTabLists()
    }

    // 履歴から全タブのカウント数を更新
    private func updateAllTabCountsFromHistory() {
        let selectedIndex = self.selectedIndex
        for i in 0 ..< MAX_TABS {
            self.selectedIndex = i
            self.updateCountsFromHistory()
        }
        self.selectedIndex = selectedIndex
    }

    // 履歴より現在タブのカウント数を更新
    private func updateCountsFromHistory() {
        let yesterday = dateAddDays(today(),days: -1)
        let tomorrow = dateAddDays(today(),days: 1)
        var count = 0.0
        var yesterdayCount = 0.0
        var totalCount = 0.0
        for idx in 0 ..< self.dailyTabListSize() {
            guard let date = dateFromString(self.dailyTabList(idx).strDate) else { continue }
            if date.compare(yesterday) != .orderedAscending && date.compare(today()) == .orderedAscending {
                yesterdayCount += self.dailyTabList(idx).count
            }
            if date.compare(today()) != .orderedAscending && date.compare(tomorrow) == .orderedAscending {
                count += self.dailyTabList(idx).count
            }
            totalCount += self.dailyTabList(idx).count
        }
        let strDate = stringFromDate(Date())
        if self.count != count || self.strDate != strDate {
            self.strTime = "-"
        }
        self.count = count
        self.strDate = strDate
        self.yesterDayCount = yesterdayCount
        self.totalCount = totalCount
    }

    func updateTimeMode(_ dailyTabCountList: Array<String>){
        for tabIdx in 0 ..< self.tabCount {
            if 0 < self.tabDatas[tabIdx].dailyList.count {
                let idx = self.tabDatas[tabIdx].dailyList[0]
                if dailyTabCountList[idx].contains(":") {
                    self.tabDatas[tabIdx].isTimeMode = true
                }
            }
        }
    }

    // CSV作成
    func exportCSV() -> Array<String> {
        var arrays = Array<Array<String>>()
        arrays.append(self.dailyDateList)
        arrays.append(self.dailyCountTextList())
        arrays.append(nameList(nameIndexList: self.dailyTabList,
                                       nameMasterList: self.tabNames))
        return csvStringArray(arrays: arrays)
    }

    // MARK: - functions - 読み込み・保存

    func loadVariable() {
        let plist = NSDictionary(contentsOfFile: documentURL(DATA_FILE_NAME).path) as? [String: Any] ?? [:]
        self.tabCount = min(plist["tabCount"] as? Int ?? DEFAULT_TABS, MAX_TABS)
        self.selectedIndex = plist["selectedIndex"] as? Int ?? 0
        if self.tabCount <= self.selectedIndex {
            self.selectedIndex = 0
        }

        for i in 0 ..< self.tabCount {
            self.tabDatas[i].count = plist["counts\(i)"] as? Double ?? 0
            self.tabDatas[i].yesterDayCount = plist["yesterDayCounts\(i)"] as? Double ?? 0
            self.tabDatas[i].totalCount = plist["totalCounts\(i)"] as? Double ?? 0
            self.tabDatas[i].strDate = plist["strDates\(i)"] as? String ?? ""
            self.tabDatas[i].strTime = plist["strTimes\(i)"] as? String ?? "-"
            self.tabDatas[i].increment = plist["increments\(i)"] as? Double ?? 1
            self.tabDatas[i].incrementHistories = plist["incrementHistories\(i)"] as? [Double] ?? Array<Double>()
            self.tabDatas[i].tabName = plist["tabNames\(i)"] as? String ?? (i + 1).description
            self.tabDatas[i].isTimeMode = plist["isTimeModes\(i)"] as? Bool ?? false
            self.tabDatas[i].isCulcAvg = plist["isCulcAvgs\(i)"] as? Bool ?? false
            self.tabDatas[i].isUpdateMode = plist["isUpdateModes\(i)"] as? Bool ?? false
            self.tabDatas[i].startDate = plist["startDates\(i)"] as? Date
            self.tabDatas[i].backColor = plist["backColors\(i)"] as? Int ?? 0
        }
        let dailyDateList = plist["dailyDateList"] as? [String] ?? Array<String>()
        let dailyCountList = plist["dailyCountList"] as? [Double] ?? Array<Double>()
        let dailyTabList = plist["dailyTabList"] as? [Int] ?? Array<Int>()
        self.dailyList.removeAll()
        for idx in 0 ..< dailyDateList.count {
            self.dailyList.append(
                DailyData(count: dailyCountList[idx], strDate: dailyDateList[idx]))
            if idx < dailyTabList.count {
                self.dailyList[idx].tab = dailyTabList[idx]
            }
        }

        // バージョンアップ用
        if dailyTabList.count == 0 {
            if (self.selectedIndex == 0) {
                self.count = plist["count"] as? Double ?? 0
                self.yesterDayCount = plist["yesterDayCount"] as? Double ?? 0
                self.totalCount = plist["totalCount"] as? Double ?? 0
                self.strDate = plist["strDate"] as? String ?? ""
                self.increment = plist["increment"] as? Double ?? 1
            }
        }

        self.updateDailyTabLists()
    }

    func saveVariable() {
        var data = [String:Any]()

        data["selectedIndex"] = self.selectedIndex
        data["tabCount"] = self.tabCount

        for i in 0 ..< self.tabCount {
            data["counts\(i)"] = self.tabDatas[i].count
            data["yesterDayCounts\(i)"] = self.tabDatas[i].yesterDayCount
            data["totalCounts\(i)"] = self.tabDatas[i].totalCount
            data["strDates\(i)"] = self.tabDatas[i].strDate
            data["strTimes\(i)"] = self.tabDatas[i].strTime
            data["increments\(i)"] = self.tabDatas[i].increment
            data["incrementHistories\(i)"] = self.tabDatas[i].incrementHistories
            data["tabNames\(i)"] = self.tabDatas[i].tabName
            data["isTimeModes\(i)"] = self.tabDatas[i].isTimeMode
            data["isCulcAvgs\(i)"] = self.tabDatas[i].isCulcAvg
            data["isUpdateModes\(i)"] = self.tabDatas[i].isUpdateMode
            data["startDates\(i)"] = self.tabDatas[i].startDate
            data["backColors\(i)"] = self.tabDatas[i].backColor
        }
        data["dailyDateList"] = self.dailyDateList
        data["dailyCountList"] = self.dailyCountList
        data["dailyTabList"] = self.dailyTabList
        (data as NSDictionary).write(to:documentURL(DATA_FILE_NAME), atomically: true)
    }
    
    // MARK: - Utility methods

    class func countText(count:Double, isTimeMode:Bool) -> String {
        if isTimeMode {
            return convStrTime(second: Int(count))
        }
        return count.dispString()
    }

}
