//
//  String+MY.swift
//  defaultApp
//
//  Created by fujii on 2021/01/09(v0.0.2).
//  Copyright © 2020 fujii. All rights reserved.
//

import UIKit

extension String {

    func substring(from: Int) -> String {
        return (self as NSString).substring(from: from) as String
    }

    func substring(to: Int) -> String {
        return (self as NSString).substring(to: to) as String
    }
    
    func isOnlyHiragana() -> Bool {
        return (self.range(of:"[^ぁ-ん]", options: .regularExpression) == nil)
    }

}

extension Array where Element == String {

    /// 重複削除
    func distinct() -> [String] {
        var allTexts = [String]()
        for text in self where !allTexts.contains(text) {
            allTexts.append(text)
        }
        return allTexts
    }

}

