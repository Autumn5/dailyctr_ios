//
//  TablistTableViewCell.swift
//  dCounterApp
//
//  Created by fujii on 2021/03/03.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

protocol TablistTableViewCellDelegate: class {
    func onTapRowCountButton(dataIndex:Int)
}

class TablistTableViewCell: UITableViewCell {
    @IBOutlet weak var tabNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var countButton: UIButton!
    @IBOutlet weak var countCoverButton: UIButton!
    @IBOutlet weak var contButtonWidthConstraint: NSLayoutConstraint!
    weak var delegate: TablistTableViewCellDelegate!
    var tabIndex: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setBackgroundImage(button:self.countButton)
    }

    func setupView(tabIndex: Int, dataModel: AppDataModel, isEditing: Bool,
                   delegate: TablistTableViewCellDelegate) {
        self.tabIndex = tabIndex
        self.delegate = delegate
        let tab = dataModel.tabDatas[tabIndex]
        self.tabNameLabel.text = tab.tabName
        self.countLabel.text = TablistViewModel.firstRowCountText(tab: tab, dataModel: dataModel)
        self.dateLabel.text = TablistViewModel.firstRowDateText(tab: tab, dataModel: dataModel)
        
        if isEditing {
            self.countButton.isHidden = false
            self.countCoverButton.isHidden = false
            self.contButtonWidthConstraint.constant = 0
        } else {
            if tab.isTimeMode || tab.isUpdateMode {
                self.countButton.isHidden = true
                self.countCoverButton.isHidden = true
            } else {
                self.countButton.isHidden = false
                self.countCoverButton.isHidden = false
                self.countButton.setTitle("+" + tab.increment.dispString(), for: .normal)
            }
            self.contButtonWidthConstraint.constant = 60
        }
    }
    
    @IBAction func onTapCountButton() {
        self.delegate.onTapRowCountButton(dataIndex: self.tabIndex)
    }

}
