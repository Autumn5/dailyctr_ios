//
//  HomeViewModel.swift
//  dCounterApp
//
//  Created by fujii on 2021/02/08.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

class HomeViewModel: NSObject {

    class func strTime(_ dataModel: AppDataModel) -> String {
        if let startDate = dataModel.startDate {
            return stringFromDate(startDate, format: "HH:mm")
        } else {
            return dataModel.strTime
        }
    }
    
    class func timeCount(_ dataModel: AppDataModel) -> Int {
        return diffSeconds(date1: dataModel.startDate!, date2: Date())
    }

    // 昨日タイトルを返す
    class func yesterdayTitle(_ countTerm: CountTermType) -> String {
        if countTerm == CountTermType.TERM_MONTHLY {
            return "今月"
        } else if countTerm == CountTermType.TERM_WEEKLY {
            return "今週"
        } else {
            return "昨日"
        }
    }

    //  合計タイトルを返す
    class func totalTitle(_ dataModel: AppDataModel, _ countTerm: CountTermType) -> String {
        if countTerm == CountTermType.TERM_MONTHLY {
            return "先月"
        } else if countTerm == CountTermType.TERM_WEEKLY {
            return "先週"
        } else {
            if dataModel.isCulcAvg {
                return "平均"
            }
            return "合計"
        }
    }

    // 今日のカウント表示文字列を返す
    class func todayCountText(_ dataModel: AppDataModel) -> String {
        if dataModel.isTimeMode {
            if dataModel.startDate != nil {
                return convStrTime(second: HomeViewModel.timeCount(dataModel))
            }
            return convStrTime(second: Int(dataModel.count))
        } else {
            return dataModel.count.dispString()
        }
    }

    // 集計カウント表示文字列を返す
    // return 表示文字列配列(昨日カウント, 合計カウント)
    class func sumCountTexts(_ dataModel: AppDataModel, _ countTerm: CountTermType) -> Array<String> {
        var yesterdayCount = 0.0
        var totalCount = 0.0
        if countTerm == CountTermType.TERM_MONTHLY {
            let counts = dataModel.countOfMonth()
            yesterdayCount = counts.0
            totalCount = counts.1
        } else if countTerm == CountTermType.TERM_WEEKLY {
            let counts = dataModel.countOfWeek()
            yesterdayCount = counts.0
            totalCount = counts.1
        } else {
            yesterdayCount = dataModel.yesterDayCount
            totalCount = dataModel.countOfAll()
        }

        var yesterdayCountText = ""
        var totalCountText = ""
        if dataModel.isTimeMode {
            yesterdayCountText = convStrTime(second: Int(yesterdayCount))
            totalCountText = convStrTime(second: Int(totalCount))
        } else {
            yesterdayCountText = yesterdayCount.dispString()
            totalCountText = totalCount.dispString()
        }
        return [yesterdayCountText, totalCountText]
    }

}
