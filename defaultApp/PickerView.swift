//
//  PickerView.swift
//  Common
//
//  Created by fujii on 2020/07/23.
//  Copyright © 2020 fujii. All rights reserved.
//

import UIKit

@objc protocol PickerViewDelegate {
    func dicidePickerInput(pickerView: PickerView)
}

class PickerView: UIView
    ,UIPickerViewDelegate ,UIPickerViewDataSource {
    @IBOutlet weak var pickerView: UIPickerView!
    private var datas: [String]!
    private var delegate: PickerViewDelegate?

    class func instance(delegate: PickerViewDelegate? = nil) -> Self {
        let view = (UIView.loadView(nibName: String(describing: self), owner: nil) as! Self)
        view.delegate = delegate
        return view
    }
    
    // MARK: - pickerView delegate

    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.datas?.count ?? 0
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return self.datas[row]
    }

    // MARK: - pickerView

    func setDatas(_ datas:[String]) {
        self.datas = datas
        self.pickerView.reloadAllComponents()
    }

    func reloadAllComponents() {
        self.pickerView.reloadAllComponents()
    }

    func numberOfRows(inComponent component: Int = 0) -> Int {
        return self.pickerView.numberOfRows(inComponent: component)
    }

    func selectedRow(inComponent component: Int = 0) -> Int {
        return self.pickerView.selectedRow(inComponent: component)
    }

    func selectedText(inComponent component: Int = 0) -> String {
        return self.datas[self.selectedRow(inComponent: component)]
    }

    func selectRow(_ row: Int, inComponent component: Int = 0) {
        self.pickerView.selectRow(row, inComponent: component, animated: false)
    }

    // MARK: - action

    @IBAction func onTapOKButton() {
        self.delegate?.dicidePickerInput(pickerView: self)
        self.removeFromSuperview()
    }
    
    @IBAction func onTapCancelButton() {
        self.removeFromSuperview()
    }

}
