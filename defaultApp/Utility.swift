//
//  Utility.swift
//  defaultApp
//
//  Created by fujii on 2021/1/23(v0.0.6).
//  Copyright © 2020 fujii. All rights reserved.
//

import UIKit

// MARK: - date

func strDateAddDays(_ strDate: String, days: Int) -> String? {
    if let date = dateFromString(strDate) {
        return stringFromDate(dateAddDays(date, days: days))
    }
    return nil
}

func dateAddDays(_ date: Date, days: Int) -> Date {
    return Calendar.current.date(byAdding: .day, value: days, to: date)!
}

func dateAddMonths(_ date: Date, months: Int) -> Date {
    return Calendar.current.date(byAdding: .month, value: months, to: date)!
}

func dateSet(_ date: Date, year: Int? = nil, month: Int? = nil, day: Int? = nil) -> Date {
    let calendar = Calendar.current
    var components = calendar.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
    if year != nil {
        components.year = year!
    }
    if month != nil {
        components.month = month!
    }
    if day != nil {
        components.day = day!
    }
    return calendar.date(from: components)!
}

// 曜日(日曜=0...土曜=6)を取得
func week(_ date: Date) -> Int {
    return Calendar.current.dateComponents([.weekday], from: date).weekday! - 1
}

func today() -> Date {
    let calendar = Calendar.current
    let components = calendar.dateComponents([.year,.month,.day], from: Date())
    return calendar.date(from: components)!
}

func dateFromString(_ strDate: String) -> Date? {
    let df = DateFormatter()
    df.locale = Locale(identifier:"ja_JP")
    df.dateFormat = "yyyy/MM/dd"
    return df.date(from:strDate)
}

func stringFromDate(_ date: Date) -> String {
    let df = DateFormatter()
    df.locale = Locale(identifier:"ja_JP")
    df.dateFormat = "yyyy/MM/dd"
    return df.string(from: date)
}

func stringFromDate(_ date: Date, format: String) -> String {
    let df = DateFormatter()
    df.locale = Locale(identifier:"ja_JP")
    df.dateFormat = format
    return df.string(from: date)
}

func diffDays(_ date1: Date, _ date2: Date) -> Int {
    let diff = date2.timeIntervalSince(date1)
    return Int(diff / 86400)
}

func diffSeconds(date1: Date, date2: Date) -> Int {
    let diff = date2.timeIntervalSince(date1)
    return Int(diff)
}

func convStrTime(second: Int) -> String {
    let hour = second / 3600
    if 0 < hour {
        return String(format:"%02d:%02d:%02d", hour, (second / 60) % 60, second % 60)
    }
    return String(format:"%02d:%02d", (second / 60) % 60, second % 60)
}

func convInt(strTime: String) -> Int? {
    var count = 0
    for str in strTime.split(separator: ":") {
        if let num = Int(String(str)) {
            count = count * 60 + num
        } else {
            return nil
        }
    }
    return count
}

func convDouble(strCount: String) -> Double {
    if strCount.contains(":") {
        return Double(convInt(strTime: strCount) ?? 0)
    }
    return Double(strCount) ?? 0
}

// MARK: - file

func documentURL(_ fileName: String) -> URL {
    let documentDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    return documentDir.appendingPathComponent(fileName)
}

func libraryURL(_ fileName: String) -> URL {
    let documentDir = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0]
    return documentDir.appendingPathComponent(fileName)
}

func saveStringArray(directory: FileManager.SearchPathDirectory, fileName: String, array: [String]) {
    let dir = FileManager.default.urls(for: directory, in: .userDomainMask)[0]
    try! array.joined(separator: "\n").write(to:dir.appendingPathComponent(fileName),
                                             atomically: true, encoding: .utf8)
}

func loadStringArray(directory: FileManager.SearchPathDirectory, fileName: String) -> [String] {
    let dir = FileManager.default.urls(for: directory, in: .userDomainMask)[0]
    return loadStringArray(url: dir.appendingPathComponent(fileName))
}

func loadResourceStringArray(name: String?, ext: String?) -> [String] {
    let url = URL(fileURLWithPath: Bundle.main.path(forResource: name, ofType: ext)!)
    return loadStringArray(url: url)
}

func loadStringArray(url: URL) -> [String] {
    if let text = try? String(contentsOf: url, encoding: .utf8) {
        var list = text.components(separatedBy: .newlines)
        list.removeAll(where:{ $0.isEmpty })
        return list
    }
    return []
}

func loadImage(url: URL) -> UIImage? {
    return UIImage(contentsOfFile: url.path)
}

func saveImage(image: UIImage, url: URL) -> Bool {
    let imageData = image.jpegData(compressionQuality: 0.9)
    do {
        try imageData!.write(to: url, options: .atomic)
    } catch {
        return false
    }
    return true
}

func deleteImage(url: URL) {
    try? FileManager.default.removeItem(atPath: url.path)
}

// MARK: - image

func resize(image: UIImage, maxWidth: CGFloat) -> UIImage {
    let width = image.size.width
    if maxWidth < width {
        let ratio = maxWidth / width
        let size = CGSize(width: width * ratio, height: image.size.height * ratio)
        UIGraphicsBeginImageContextWithOptions(size, false, 1.0)
        image.draw(in: CGRect(origin: .zero, size: size))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return resizedImage
    }
    return image
}

// MARK: - string

func filterTexts(_ texts: Array<String>, searchText: String) -> Array<String> {
    if 0 == searchText.count {
        return texts
    }
    var list = Array<String>()
    for text in texts {
        if (text.contains(searchText)) {
            list.append(text)
        }
    }
    return list
}


// MARK: - array

// CSV配列を返す
func csvStringArray(arrays: [[String]]) -> [String] {
    var array = Array<String>()
    for row in 0 ..< arrays[0].count {
        var lineArray = Array<String>()
        for idx in 0 ..< arrays.count {
            lineArray.append(arrays[idx][row])
        }
        array.append(lineArray.joined(separator: ","))
    }
    return array
}

func stringArrays(csvArray: [String]) -> [[String]] {
    var arrays = Array<Array<String>>()
    for (row, line) in csvArray.enumerated() {
        if line.isEmpty { continue }
        let items = line.components(separatedBy: ",")
        if row == 0 {
            for _ in 0 ..< items.count {
                arrays.append(Array<String>())
            }
        }
        for idx in 0 ..< arrays.count {
            let item = idx < items.count ? items[idx] : ""
            arrays[idx].append(item)
        }
    }
    return arrays
}

func stringList(intList: Array<Int>) -> Array<String> {
    var list = Array<String>()
    for num in intList {
        list.append(num.description)
    }
    return list
}

func stringList(doubleList: Array<Double>) -> Array<String> {
    var list = Array<String>()
    for num in doubleList {
        if num > num.rounded(.down) {
            list.append(num.description)
        } else {
            list.append(Int(num).description)
        }
    }
    return list
}

func stringList(count: Int, value: String = "") -> Array<String> {
    var list = Array<String>()
    for _ in 0 ..< count {
        list.append(value)
    }
    return list
}

func intList(stringList: Array<String>) -> Array<Int> {
    var list = Array<Int>()
    for string in stringList {
        list.append(Int(string)!)
    }
    return list
}

func intList(count: Int, value: Int = 0) -> Array<Int> {
    var list = Array<Int>()
    for _ in 0 ..< count {
        list.append(value)
    }
    return list
}

// 名前マスタ配列のインデックス配列を返す
func indexList(nameList: Array<String>, nameMasterList: Array<String>,
              defaultValue: Int = -1) -> Array<Int> {
    var indexList = Array<Int>()
    for name in nameList {
        let idx = nameMasterList.firstIndex(of: name) ?? defaultValue
        indexList.append(idx)
    }
    return indexList
}

// 名前マスタ配列のインデックス配列を名前配列に変換する
func nameList(nameIndexList: Array<Int>, nameMasterList: Array<String>,
             defaultValue: String = "") -> Array<String> {
    var nameList = Array<String>()
    for nameIndex in nameIndexList {
        if 0 <= nameIndex && nameIndex < nameMasterList.count {
            nameList.append(nameMasterList[nameIndex])
        } else {
            nameList.append(defaultValue)
        }
    }
    return nameList
}

// MARK: - other

func simpleAlert(title: String, message: String) -> UIAlertController {
    let alert = UIAlertController(title: title,
                                  message: message,
                                  preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler:nil))
    return alert
}

func Utility_toolBar(withButtonTexts buttonTexts:[String], target:Any, action:Selector) -> UIToolbar {
    let toolBar = UIToolbar()
    toolBar.barStyle = UIBarStyle.default
    var barButtonItems = [UIBarButtonItem]()
    for text in buttonTexts {
        if text == "FlexibleSpace" {
            barButtonItems.append(UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace,
                                        target: nil, action: nil))
        } else {
            barButtonItems.append(UIBarButtonItem.init(title: text,
                                                       style: .plain,
                                                       target: target,
                                                       action: action))
        }
    }
    toolBar.items = barButtonItems
    toolBar.sizeToFit()
    return toolBar
}
