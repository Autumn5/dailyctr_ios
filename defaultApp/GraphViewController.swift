//
//  GraphViewController.swift
//  dCounterApp
//
//  Created by fujii on 2021/01/23.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

class GraphViewController: UIViewController {
    @IBOutlet weak var graphView: GraphView!
    @IBOutlet weak var yMaxTextView: UILabel!
    @IBOutlet weak var yMinTextView: UILabel!
    @IBOutlet weak var xMaxTextView: UILabel!
    @IBOutlet weak var xMinTextView: UILabel!
    @IBOutlet weak var tabSegment: UISegmentedControl!
    public var countTerm = CountTermType.TERM_DAYLY
    var dataModel = AppDataModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataModel.loadVariable()
        self.updateDisplay()
    }

    // MARK: - actions
    
    @IBAction func onSelectedTabSegment(_ segmentedControl: UISegmentedControl) {
        self.countTerm = CountTermType.init(rawValue: segmentedControl.selectedSegmentIndex)!
        self.updateDisplay()
    }

    // MARK: - functions

    func updateDisplay() {
        var graphArray : Array<GraphPoint>
        if countTerm == CountTermType.TERM_DAYLY {
            graphArray = GraphModel.createGraphDailyArray(dataModel)
        } else if countTerm == CountTermType.TERM_WEEKLY {
            graphArray = GraphModel.createGraphWeekArray(dataModel)
        } else {
            graphArray = GraphModel.createGraphMonthArray(dataModel)
        }
        self.tabSegment.selectedSegmentIndex = countTerm.rawValue
        yMaxTextView.text = GraphModel.yMaxText(self.dataModel, graphArray)
        yMinTextView.text = GraphModel.yMinText(self.dataModel, graphArray)
        xMaxTextView.text = graphArray.last?.xLabel ?? ""
        xMinTextView.text = graphArray.first?.xLabel ?? ""
        self.graphView.setGraphArray(graphArray)
        self.graphView.setNeedsDisplay()
    }

}
