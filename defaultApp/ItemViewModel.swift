//
//  ItemViewModel.swift
//  dCounterApp
//
//  Created by fujii on 2021/03/13.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

class ItemViewModel {

    // ヘッダー：件数・平均日数
    class func headerCountText(dataModel: AppDataModel, dateInfo: AppDataModel.DateInfo) -> String {
        if dataModel.dailyTabListSize() == 0 {
            return ""
        }
        let avgDays = Double(dateInfo.diffDays) / Double(dataModel.dailyTabListSize())
        return String(format: "%d件(平均%@日/件)"
                , dataModel.dailyTabListSize()
                , avgDays.dispString())
    }

    // ヘッダー：日付
    class func headerDateTermText(dateInfo: AppDataModel.DateInfo) -> String {
        return String(format: "%@〜%@", dateInfo.minStrDate, dateInfo.maxStrDate)
    }

}
