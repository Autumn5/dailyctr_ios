//
//  PickerViewColorDelegate.swift
//  dCounterApp
//
//  Created by fujii on 2021/02/26.
//  Copyright © 2021 fujii. All rights reserved.
//

import UIKit

class PickerViewColorDelegate: NSObject
    , UIPickerViewDelegate {
    var colors: Array<Int>!
    weak var pickerView: PickerView!

    class func instance(pickerView: PickerView, colors: Array<Int>) -> Self {
        let instance = PickerViewColorDelegate() as! Self
        instance.pickerView = pickerView
        instance.pickerView.pickerView.delegate = instance
        instance.colors = colors
        return instance
    }
    
    // MARK: - pickerView delegate

    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return self.pickerView.numberOfComponents(in: pickerView)
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.pickerView.pickerView(pickerView, numberOfRowsInComponent: component)
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = (view as? UILabel) ?? UILabel()
        pickerLabel.text = self.pickerView.pickerView(pickerView, titleForRow: row, forComponent: component)
        pickerLabel.textAlignment = .center
        pickerLabel.backgroundColor = backColor(self.colors[row])
        return pickerLabel
    }
}
