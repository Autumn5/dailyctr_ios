//
//  defaultAppTests.swift
//  defaultAppTests
//
//  Created by fujii on 2021/01/07.
//  Copyright © 2021 fujii. All rights reserved.
//

import XCTest
@testable import dCounterApp

class defaultAppTests: XCTestCase {

    override func setUp() {
    }

    override func tearDown() {
    }

    func test_strDateAddDays() {
        let strDate = strDateAddDays("2021/1/1", days: 1)
        XCTAssertEqual("2021/01/02", strDate)
    }

    func test_stringFromDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"

        let sDate = formatter.date(from: "2020/01/01 00:01")
        XCTAssertEqual("2020/01/01", stringFromDate(sDate!))

        let eDate = formatter.date(from: "2020/12/31 23:59")
        XCTAssertEqual("2020/12/31", stringFromDate(eDate!))
    }

    func test_indexList() {
        var strArray = Array<String>()
        strArray.append("tab1")
        strArray.append("tab3")
        strArray.append("tab5")
        var strMasterArray = Array<String>()
        strMasterArray.append("tab1")
        strMasterArray.append("tab2")
        strMasterArray.append("tab3")
        let array = indexList(nameList: strArray, nameMasterList: strMasterArray)
        XCTAssertEqual(array[0], 0)
        XCTAssertEqual(array[1], 2)
        XCTAssertEqual(array[2], -1)
    }

    func testPerformanceExample() {
        measure {
        }
    }

}
