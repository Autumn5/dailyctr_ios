//
//  CounterTests.swift
//  defaultAppTests
//
//  Created by fujii on 2021/01/10.
//  Copyright © 2021 fujii. All rights reserved.
//

import XCTest
@testable import dCounterApp

class CounterTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_dataModel() {
        var dataModel = AppDataModel()

        dataModel.updateCount(increment: 1.0)
        XCTAssertEqual(dataModel.count, 1.0)
        XCTAssertEqual(dataModel.totalCount, 1.0)

        // 更新モード
        dataModel.isUpdateMode = true
        dataModel.updateCount(increment: 5.0)
        XCTAssertEqual(dataModel.count, 5.0)
        XCTAssertEqual(dataModel.totalCount, 5.0)
        
        // クリア
        dataModel.countClear()
        XCTAssertEqual(dataModel.count, 0)
        XCTAssertEqual(dataModel.totalCount, 0)
        XCTAssertEqual(dataModel.countOfAll(), 0)

        // タブ指定カウント
        dataModel = AppDataModel()
        dataModel.updateCount(increment: 1.0)
        dataModel.updateCount(increment: 1.5)
        dataModel.updateCount(tabIndex: 1, increment: 10.0)
        XCTAssertEqual(dataModel.count, 2.5)
        XCTAssertEqual(dataModel.totalCount, 2.5)
        XCTAssertEqual(dataModel.tabDatas[1].countText(), "10")
    }

    func test_dataModel_importCSV() {
        var dataModel = AppDataModel()

        var csvArray = Array<String>()
        csvArray.append("2020/01/01,1,tab1")
        csvArray.append("2020/01/02,1,tab1")
        assert(dataModel.importCSV(csvArray: csvArray))
        dataModel.selectedIndex = 0
        XCTAssertEqual(dataModel.totalCount, 2)
        XCTAssertEqual(dataModel.tabCount, DEFAULT_TABS)

        // 昨日・今日
        csvArray.removeAll()
        csvArray.append(stringFromDate(today()) + ",2,tab2")
        csvArray.append(stringFromDate(dateAddDays(today(),days: -1)) + ",1,tab2")
        csvArray.append(stringFromDate(today()) + ",1,tab1")
        assert(dataModel.importCSV(csvArray: csvArray))
        dataModel.selectedIndex = 1
        XCTAssertEqual(dataModel.count, 2)
        XCTAssertEqual(dataModel.yesterDayCount, 1)

        // 全集計・月集計
        csvArray.removeAll()
        csvArray.append(stringFromDate(dateSet(today(), day: 1)) + ",2,tab1")
        csvArray.append(stringFromDate(dateAddMonths(today(),  months: -1)) + ",3,tab1")
        assert(dataModel.importCSV(csvArray: csvArray))
        dataModel.selectedIndex = 0
        let count = dataModel.countOfAll()
        XCTAssertEqual(count, 5.0)
        var counts = dataModel.countOfMonth()
        XCTAssertEqual(counts.0, 2)
        XCTAssertEqual(counts.1, 3)
        
        // 全集計・月集計：平均
        csvArray.removeAll()
        csvArray.append(stringFromDate(dateSet(today(), day: 1)) + ",2,tab1")
        csvArray.append(stringFromDate(dateSet(today(), day: 2)) + ",4,tab1")
        assert(dataModel.importCSV(csvArray: csvArray))
        dataModel.isCulcAvg = true
        XCTAssertEqual(dataModel.totalCount, 6.0)
        XCTAssertEqual(dataModel.countOfAll(), 3.0)
        counts = dataModel.countOfMonth()
        XCTAssertEqual(counts.0, 3.0)

        // 日付フォーマット
        csvArray.removeAll()
        csvArray.append("2020/1/1,2,tab1")
        csvArray.append("2020年元旦,2,tab1")
        assert(dataModel.importCSV(csvArray: csvArray))
        dataModel.selectedIndex = 0
        XCTAssertEqual(dataModel.dailyDateList[0], "2020/01/01")
        XCTAssertEqual(dataModel.dailyDateList[1], "2020年元旦")
        
        // タブ数増減(初期設定)
        dataModel = AppDataModel()
        csvArray.removeAll()
        csvArray.append("2020/1/1,2,tab2")
        assert(dataModel.importCSV(csvArray: csvArray))

        // タブ数増
        csvArray.removeAll()
        csvArray.append("2020/1/1,2,tab1")
        csvArray.append("2020/1/1,2,tab2")
        csvArray.append("2020/1/1,2,tab3")
        csvArray.append("2020/1/1,2,tab4")
        csvArray.append("2020/1/1,2,tab5")
        assert(dataModel.importCSV(csvArray: csvArray))
        XCTAssertEqual(dataModel.tabCount, 5)
        XCTAssertEqual(dataModel.tabNames[0], "tab2")

        // タブ数減
        csvArray.removeAll()
        csvArray.append("2020/1/1,2,tab1")
        csvArray.append("2020/1/1,2,tab3")
        csvArray.append("2020/1/1,2,tab5")
        assert(dataModel.importCSV(csvArray: csvArray))
        XCTAssertEqual(dataModel.tabCount, 3)
        XCTAssertEqual(dataModel.tabNames[0], "tab5")
        XCTAssertEqual(dataModel.tabNames[1], "tab1")
        XCTAssertEqual(dataModel.tabNames[2], "tab3")

        // 時間モード
        dataModel = AppDataModel()
        csvArray.removeAll()
        csvArray.append(stringFromDate(today()) + ",62,tab1")
        assert(dataModel.importCSV(csvArray: csvArray))
        dataModel.isTimeMode = true
        XCTAssertEqual(dataModel.dailyTabListCountText(0),"01:02")
        
        // 時間モード解析
        dataModel = AppDataModel()
        csvArray.removeAll()
        csvArray.append("2020/01/02,01:59,tab1")
        assert(dataModel.importCSV(csvArray: csvArray))
        XCTAssertEqual(dataModel.isTimeMode, true)
    }

    func test_dataModel_exportCSV() {
        let dataModel = AppDataModel()
        var csvArray = Array<String>()
        csvArray.append("2020/01/01,1,tab1")
        csvArray.append("2020/01/02,1.01,tab1")
        csvArray.append("2020/01/02,1.99,tab1")
        csvArray.append("2020/01/02,01:59,tabt")
        csvArray.append("2020/01/02,00:01,tabt")
        assert(dataModel.importCSV(csvArray: csvArray))

        let exportCsvArray = dataModel.exportCSV()
        XCTAssertEqual(csvArray[0], exportCsvArray[0])
        XCTAssertEqual(csvArray[1], exportCsvArray[1])
        XCTAssertEqual(csvArray[2], exportCsvArray[2])
        XCTAssertEqual(csvArray[3], exportCsvArray[3])
        XCTAssertEqual(csvArray[4], exportCsvArray[4])
    }
    
    func test_dataModel_createQR() {
        let dataModel = AppDataModel()

        dataModel.strDate = "2020/01/02"
        dataModel.strTime = "10:11"
        dataModel.count = 1
        dataModel.yesterDayCount = 2
        dataModel.totalCount = 3
        var qrData = dataModel.createQRData()
        XCTAssertEqual(qrData.qrString,"2020/01/02,10:11,1,2,3,{}")

        dataModel.addOrUpdateDailyCount("2020/02/03",4)
        dataModel.addOrUpdateDailyCount("2020/03/04",5)
        qrData = dataModel.createQRData()
        XCTAssertEqual(qrData.qrString,"2020/01/02,10:11,1,2,3,{2020/02/03,4,2020/03/04,5}")
    }

    func test_dataModel_importQR() {
        let dataModel = AppDataModel()
        dataModel.addOrUpdateDailyCount("2020/03/02",10)
        dataModel.addOrUpdateDailyCount("2020/03/04",20)

        assert(dataModel.importQR(strData: "2020/01/02,10:11,1,2,3,{2020/02/03,4,2020/03/04,5}"))
        XCTAssertEqual(dataModel.strDate, "2020/01/02")
        XCTAssertEqual(dataModel.strTime, "10:11")
        XCTAssertEqual(dataModel.count, 1)
        XCTAssertEqual(dataModel.yesterDayCount, 2)
        XCTAssertEqual(dataModel.totalCount, 3.0)
        XCTAssertEqual(dataModel.countOfAll(), 3.0)
        XCTAssertEqual(dataModel.dailyTabList(2).strDate,"2020/02/03")
        XCTAssertEqual(dataModel.dailyTabList(2).count,4)
        XCTAssertEqual(dataModel.dailyTabList(1).strDate,"2020/03/04")
        XCTAssertEqual(dataModel.dailyTabList(1).count,5)

        dataModel.isCulcAvg = true
        dataModel.addOrUpdateDailyCount("2020/03/05",2)
        XCTAssertEqual(dataModel.countOfAll(), 5.25)
    }
    
    func test_dataModel_insertTab() {
        let dataModel = AppDataModel()
        dataModel.selectedIndex = 2
        dataModel.addOrUpdateDailyCount("2020/03/02",10)
        dataModel.tabName = "test"

        dataModel.selectedIndex = 0
        dataModel.insertTab()
        dataModel.insertTab()
        dataModel.insertTab()
        XCTAssertEqual(dataModel.tabCount, DEFAULT_TABS + 3)
        dataModel.selectedIndex = 5
        XCTAssertEqual(dataModel.dailyTabListSize(), 1)
        XCTAssertEqual(dataModel.tabName, "test")
        
        dataModel.selectedIndex = 0
        dataModel.removeTabAndList()
        dataModel.removeTabAndList()
        dataModel.removeTabAndList()
        XCTAssertEqual(dataModel.tabCount, 3)
        dataModel.selectedIndex = 2
        XCTAssertEqual(dataModel.dailyTabListSize(), 1)
        XCTAssertEqual(dataModel.tabName, "test")
        dataModel.removeTabAndList()
        dataModel.removeTabAndList()
        dataModel.removeTabAndList()
        XCTAssertEqual(dataModel.tabCount, 1)
        XCTAssertEqual(dataModel.tabName, "1")
        
        for _ in dataModel.tabCount ... MAX_TABS {
            dataModel.insertTab()
        }
        XCTAssertEqual(dataModel.tabCount, MAX_TABS)
    }

    func test_dataModel_moveTab() {
        var dataModel : AppDataModel!
        func reset(){
            dataModel = AppDataModel()
            dataModel.selectedIndex = 0
            dataModel.addOrUpdateDailyCount("2020/01/01",1.0)
            dataModel.tabName = "tab1"
            dataModel.selectedIndex = 1
            dataModel.addOrUpdateDailyCount("2020/02/01",2.0)
            dataModel.tabName = "tab2"
            dataModel.selectedIndex = 2
            dataModel.addOrUpdateDailyCount("2020/03/01",3.0)
            dataModel.tabName = "tab3"
            dataModel.selectedIndex = 3
            dataModel.addOrUpdateDailyCount("2020/04/01",4.0)
            dataModel.tabName = "tab4"
        }

        reset()
        dataModel.moveTab(fromIndex: 0,toIndex: 2)
        dataModel.selectedIndex = 0
        XCTAssertEqual(dataModel.tabName, "tab2")
        XCTAssertEqual(dataModel.dailyTabList(0).count,2.0)
        dataModel.selectedIndex = 1
        XCTAssertEqual(dataModel.tabName, "tab3")
        XCTAssertEqual(dataModel.dailyTabList(0).count,3.0)
        dataModel.selectedIndex = 2
        XCTAssertEqual(dataModel.tabName, "tab1")
        XCTAssertEqual(dataModel.dailyTabList(0).count,1.0)
        dataModel.selectedIndex = 3
        XCTAssertEqual(dataModel.tabName, "tab4")
        XCTAssertEqual(dataModel.dailyTabList(0).count,4.0)

        reset()
        dataModel.moveTab(fromIndex: 3,toIndex: 1)
        dataModel.selectedIndex = 0
        XCTAssertEqual(dataModel.tabName, "tab1")
        XCTAssertEqual(dataModel.dailyTabList(0).count,1.0)
        dataModel.selectedIndex = 1
        XCTAssertEqual(dataModel.tabName, "tab4")
        XCTAssertEqual(dataModel.dailyTabList(0).count,4.0)
        dataModel.selectedIndex = 2
        XCTAssertEqual(dataModel.tabName, "tab2")
        XCTAssertEqual(dataModel.dailyTabList(0).count,2.0)
        dataModel.selectedIndex = 3
        XCTAssertEqual(dataModel.tabName, "tab3")
        XCTAssertEqual(dataModel.dailyTabList(0).count,3.0)
    }

    // HomeViewModel

    func test_HomeViewModel_count() {
        // 今日・昨日・合計
        var dataModel = AppDataModel()
        var csvArray = Array<String>()
        csvArray.append("2020/01/01,60,tab1")
        csvArray.append(stringFromDate(dateAddDays(today(),days: -1)) + ",2,tab1")
        csvArray.append(stringFromDate(today()) + ",3,tab1")
        assert(dataModel.importCSV(csvArray: csvArray))

        XCTAssertEqual(HomeViewModel.todayCountText(dataModel), "3")
        var sumCountTexts = HomeViewModel.sumCountTexts(dataModel, CountTermType.TERM_DAYLY)
        XCTAssertEqual(sumCountTexts[0],"2")
        XCTAssertEqual(sumCountTexts[1],"65")
        XCTAssertEqual(HomeViewModel.yesterdayTitle(CountTermType.TERM_DAYLY),"昨日")
        XCTAssertEqual(HomeViewModel.totalTitle(dataModel, CountTermType.TERM_DAYLY),"合計")

        dataModel.isTimeMode = true
        XCTAssertEqual(HomeViewModel.todayCountText(dataModel), "00:03")
        sumCountTexts = HomeViewModel.sumCountTexts(dataModel, CountTermType.TERM_DAYLY)
        XCTAssertEqual(sumCountTexts[0],"00:02")
        XCTAssertEqual(sumCountTexts[1],"01:05")

        // 今月・先月
        dataModel = AppDataModel()
        csvArray.removeAll()
        csvArray.append(stringFromDate(dateSet(today(), day: 1)) + ",2,tab1")
        csvArray.append(stringFromDate(dateSet(today(), day: 2)) + ",3,tab1")
        csvArray.append(stringFromDate(dateAddMonths(today(), months: -1)) + ",1,tab1")
        assert(dataModel.importCSV(csvArray: csvArray))
        
        sumCountTexts = HomeViewModel.sumCountTexts(dataModel, CountTermType.TERM_MONTHLY)
        XCTAssertEqual(sumCountTexts[0],"5")
        XCTAssertEqual(sumCountTexts[1],"1")
        XCTAssertEqual(HomeViewModel.yesterdayTitle(CountTermType.TERM_MONTHLY),"今月")
        XCTAssertEqual(HomeViewModel.totalTitle(dataModel, CountTermType.TERM_MONTHLY),"先月")
    }
    
    // GraphModel

    func test_GraphModel_createGraphArray() {
        var dataModel = AppDataModel()
        var graphArray = GraphModel.createGraphDailyArray(dataModel)
        XCTAssertEqual(graphArray.count, 0)

        dataModel.addOrUpdateDailyCount("2020/03/02", 10)
        dataModel.addOrUpdateDailyCount("2020/03/07", 20)
        dataModel.addOrUpdateDailyCount("2020/03/08", 40)
        dataModel.addOrUpdateDailyCount("2020/04/01", 30)

        graphArray = GraphModel.createGraphDailyArray(dataModel)
        XCTAssertEqual(graphArray.count, 4)
        XCTAssertEqual(graphArray[0].y, 10)
        XCTAssertEqual(graphArray[1].x, 5)
        XCTAssertEqual(GraphModel.yMaxText(dataModel, graphArray), "40")
        XCTAssertEqual(GraphModel.yMinText(dataModel, graphArray), "0")

        graphArray = GraphModel.createGraphWeekArray(dataModel)
        XCTAssertEqual(graphArray.count, 3)
        XCTAssertEqual(graphArray[0].y, 30)
        XCTAssertEqual(graphArray[1].x, 7)

        graphArray = GraphModel.createGraphMonthArray(dataModel)
        XCTAssertEqual(graphArray.count, 2)
        XCTAssertEqual(graphArray[0].y, 70)
        XCTAssertEqual(graphArray[1].x, 31)
        
        // MIN/MAX
        dataModel = AppDataModel()
        dataModel.addOrUpdateDailyCount("2020/03/01", 100.0)
        dataModel.addOrUpdateDailyCount("2020/03/02", 60.0)
        graphArray = GraphModel.createGraphDailyArray(dataModel)
        XCTAssertEqual(GraphModel.yMaxText(dataModel, graphArray), "100")
        XCTAssertEqual(GraphModel.yMinText(dataModel, graphArray), "60")

        dataModel.addOrUpdateDailyCount("2020/03/03", 40.0)
        graphArray = GraphModel.createGraphDailyArray(dataModel)
        XCTAssertEqual(GraphModel.yMinText(dataModel, graphArray), "0")
    }
    
    // TablistViewModel

    func test_TablistViewModel() {
        let dataModel = AppDataModel()
        dataModel.addOrUpdateDailyCount("2021/02/01", 50.0)
        dataModel.addOrUpdateDailyCount(stringFromDate(dateSet(today(), month: 3, day: 1)), 100.0)

        var tab = dataModel.tabDatas[0]
        XCTAssertEqual(TablistViewModel.firstRowCountText(tab: tab, dataModel: dataModel),"100")
        XCTAssertEqual(TablistViewModel.firstRowDateText(tab: tab, dataModel: dataModel),"03/01")

        // 今日
        dataModel.selectedIndex = 1
        dataModel.addOrUpdateDailyCount(stringFromDate(today()), 60.5)
        dataModel.selectedIndex = 0
        tab = dataModel.tabDatas[1]
        XCTAssertEqual(TablistViewModel.firstRowCountText(tab: tab, dataModel: dataModel),"60.5")
        XCTAssertEqual(TablistViewModel.firstRowDateText(tab: tab, dataModel: dataModel),"今日")

    }
    
    // ItemViewModel

    func test_ItemViewModel() {
        let dataModel = AppDataModel()
        var dateInfo = dataModel.dateInfoOfAll()
        XCTAssertEqual(ItemViewModel.headerCountText(dataModel: dataModel, dateInfo: dateInfo), "")
        XCTAssertEqual(ItemViewModel.headerDateTermText(dateInfo: dateInfo),"〜")
        XCTAssertEqual(AppDataModel.countText(count: dataModel.totalCount, isTimeMode: dataModel.isTimeMode),"0")
        XCTAssertEqual(AppDataModel.countText(count: dataModel.averageOfAll(), isTimeMode: dataModel.isTimeMode),"0")
        XCTAssertEqual(AppDataModel.countText(count: dataModel.minOfAll(), isTimeMode: dataModel.isTimeMode),"0")
        XCTAssertEqual(AppDataModel.countText(count: dataModel.maxOfAll(), isTimeMode: dataModel.isTimeMode),"0")

        var csvArray = Array<String>()
        csvArray.append("2021/01/01,50,tab1")
        csvArray.append("2021/02/01,100,tab1")
        csvArray.append("2021/03/01,200,tab2")
        assert(dataModel.importCSV(csvArray: csvArray))
        dateInfo = dataModel.dateInfoOfAll()
        XCTAssertEqual(ItemViewModel.headerCountText(dataModel: dataModel, dateInfo: dateInfo), "2件(平均16日/件)")
        XCTAssertEqual(ItemViewModel.headerDateTermText(dateInfo: dateInfo),"2021/01/01〜2021/02/01")
        XCTAssertEqual(AppDataModel.countText(count: dataModel.totalCount, isTimeMode: dataModel.isTimeMode),"150")
        XCTAssertEqual(AppDataModel.countText(count: dataModel.averageOfAll(), isTimeMode: dataModel.isTimeMode),"75")
        XCTAssertEqual(AppDataModel.countText(count: dataModel.minOfAll(), isTimeMode: dataModel.isTimeMode),"50")
        XCTAssertEqual(AppDataModel.countText(count: dataModel.maxOfAll(), isTimeMode: dataModel.isTimeMode),"100")

        dataModel.isTimeMode = true
        XCTAssertEqual(AppDataModel.countText(count: dataModel.totalCount, isTimeMode: dataModel.isTimeMode),"02:30")
        XCTAssertEqual(AppDataModel.countText(count: dataModel.averageOfAll(), isTimeMode: dataModel.isTimeMode),"01:15")
        XCTAssertEqual(AppDataModel.countText(count: dataModel.minOfAll(), isTimeMode: dataModel.isTimeMode),"00:50")
        XCTAssertEqual(AppDataModel.countText(count: dataModel.maxOfAll(), isTimeMode: dataModel.isTimeMode),"01:40")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
